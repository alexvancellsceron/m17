using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

// s el sistema de parallelitzaci de Unity, cal indicar
// que volem que l'utilitzi i cal posar-ho a tota funci
[BurstCompile]
partial struct SpawnerSystem : ISystem
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var ecbSingleton = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>();
        EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

        foreach(RefRW<Spawner> s in SystemAPI.Query <RefRW<Spawner>>())
        {
            Entity sapwnedEntity = ecb.Instantiate(s.ValueRO.entityPrefab);
            ecb.SetComponent(sapwnedEntity, new Speed
            {
                speed = 4,
                direction = new float3(0, -10, 0)
            });

            ecb.SetComponent(sapwnedEntity, WorldTransform.FromPosition(new Unity.Mathematics.float3(0, 0, 0)));
        }

    }
}

//Aquest s el joc que executar les tasques del nostre sistema ^^
[BurstCompile]
partial struct SpawnerJob : IJobEntity
{
    public EntityCommandBuffer ECB;
    public float deltaTime;

    //Com a parmetres de l'execute vindriem a posar les condicions de cerca
    //del nostre sistema. En el nostre cas agafem l'aspecte
    //in -> read-only
    //ref -> read-write
    void Execute(ref SpawnerAspect spawner)
    {
        spawner.ElapseTime(deltaTime, ECB);
    }
}
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public struct Spawner : IComponentData
{
    public Entity entityPrefab;
    public float spawnRate;
    //Com ara tenim sistemes i les entitats guarden les dades,
    //el temps d'spawn ser una dada a desar al seu component
    public float elapsedTime;
    //Per a fer random, cal desar l'estat del random en dades
    public Unity.Mathematics.Random random;
}

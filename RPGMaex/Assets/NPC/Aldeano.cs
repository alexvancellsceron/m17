using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aldeano : MonoBehaviour
{

    public NonPlayableCharacters texto;
    public QuestSO quest;
    public delegate void mostrarCanvasTexto(string s);
    public event mostrarCanvasTexto mostrarTextoDelegator;

    public void accionar()
    {
        if(!quest.collected)
        {
            mostrarTextoDelegator.Invoke(texto.frase);
        }
        else
        {
            this.texto.frase = "OH VAYA, ESA LLAVE!!!... La perd� buscando caracoles en aquella cueva, la verdad" +
                "es que no abre nada, pero se ve bonita �Verdad?";
            mostrarTextoDelegator.Invoke(texto.frase);
        }

    }





}

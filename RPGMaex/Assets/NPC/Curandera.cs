using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Curandera : MonoBehaviour
{
    public PlayableCharacters prota1, prota2;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            prota1.setCurrentHP(prota1.m_MaxHP);
            prota1.setCurrentPP(prota1.m_MaxPP);
            prota2.setCurrentHP(prota2.m_MaxHP);
            prota2.setCurrentPP(prota2.m_MaxPP);
        }
    }
}

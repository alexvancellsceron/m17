using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Armor : RPGItem, Equipable, Buyable
{
    public float m_defense;
    public float m_sellingPrice;
    public float m_buyingPrice;



    public float SellingPrice => m_sellingPrice;
    public float BuyingPrice => m_buyingPrice;

    public void buy()
    {
        //TODO
        throw new System.NotImplementedException();
    }

    public void equip(PlayableCharacters c)
    {
        //TODO
        throw new System.NotImplementedException();
    }

    public void sell()
    {
        //TODO
        throw new System.NotImplementedException();
    }

    public void unequip()
    {
        //TODO
        throw new System.NotImplementedException();
    }

}

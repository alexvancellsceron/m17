using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]

public class Potion : RPGItem, Consumable, Buyable
{
    public float m_effectPower;
    public Stats[] m_affectedStat;
    public float m_buyingPrice;
    public float m_sellingPrice;
    public GameEventItem m_geiResta;
    public GameEventItem m_geiSuma;
    public GameEventShopping m_ges;
    public float BuyingPrice => -m_buyingPrice;
    public float SellingPrice => m_sellingPrice;


    public void consume(Characters c)
    {
        if(m_affectedStat != null)
        {
            foreach (Stats stat in m_affectedStat)
            {
                c.setStat(m_effectPower, stat);
                m_geiResta.Raise(this);
                Debug.Log(c.getCurrentHP());
            }
        }


    }

    public void sell()
    {

        m_geiResta.Raise(this);
        //Event to setMoney(float money) -> money is possitive because you are GAINING money from the sell
        //m_shopping.Raise(SellingPrice);
        
    }

    public void buy()
    {
        m_geiSuma.Raise(this);
        //Event to setMoney(float money) -> moneys is negative because you are LOSING money from the buy
        //m_shopping.Raise(BuyingPrice);

    }
}

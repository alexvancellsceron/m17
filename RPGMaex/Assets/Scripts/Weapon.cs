using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum WeaponTypes{
    SWORD,GREAT_SWORD,FLORETTE,LANCE,DAGGER,QUARTERSTAFF,MACE,HUMMER,SCYTHE,SICKLE,SAI
}
[CreateAssetMenu]
[Serializable]
public class Weapon : RPGItem, Equipable, Buyable
{
    public float m_attk;
    public float m_dexterityLvl;
    public float m_buyingPrice;
    public float m_sellingPrice;
    public GameEventItem geiAdd;
    public GameEventItem geiRemove;
    public float BuyingPrice => m_buyingPrice;
    public float SellingPrice => m_sellingPrice;
    WeaponAbility m_ability;
    public WeaponTypes m_weaponType;



    public void buy()
    {
        //TODO
        throw new System.NotImplementedException();
    }

    public void equip(PlayableCharacters c)
    {
        if(c.m_equipedWeapon==null)
        {
            geiRemove.Raise(this);
            c.m_equipedWeapon = this;
        }
        else
        {
            geiRemove.Raise(this);
            geiAdd.Raise(c.m_equipedWeapon);         
            c.m_equipedWeapon = this;
        }
    }

    public void sell()
    {
        //TODO
        throw new System.NotImplementedException();
    }

    public void unequip()
    {
        //TODO
        throw new System.NotImplementedException();
    }
}

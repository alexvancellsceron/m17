using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class QuestSO : ScriptableObject
{
    public RPGItem item;
    public bool collected;

}

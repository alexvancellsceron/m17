using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class NonPlayableCharacters : Characters
{
    public List<RPGItem> items;
    public GameEventItem gei;
    public bool haveItem = true;
    public string frase;    
    public void give()
    {
        foreach (RPGItem item in items)
        {
            gei.Raise(item); //Inventory.Add(item);

        }
    }


}

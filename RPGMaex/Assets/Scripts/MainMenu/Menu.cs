using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject FMain, FOptions, FChoose, FPlay, Fondo1, Fondo2, Fondo3, FondoButtons, Slider, DD;
    public AudioSource audioSource;
    public AudioClip[] clip;
    public Image transicion, imageControls;
    public Sprite imageKeyboard, imageGamepad;

    // Start is called before the first frame update
    void Start()
    {
        Slider.GetComponent<Slider>().value = 50f;
        imageControls.sprite = imageKeyboard;
    }

    public void Selection(int i)
    {
        switch(i)
        {
            case 0:
                if(Fondo3.activeSelf == false)
                {
                    FondoButtons.SetActive(false);
                    audioSource.PlayOneShot(clip[1]);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(FChoose);
                    Fondo3.SetActive(true);
                }
                else{
                    FondoButtons.SetActive(true);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(FPlay);
                    audioSource.PlayOneShot(clip[0]);
                    Fondo3.SetActive(false);
                }
                break;
            case 1:
                break;
            case 2:
                if(Fondo1.activeSelf == true)
                {
                    audioSource.PlayOneShot(clip[0]);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(FOptions);
                    StartCoroutine(fade(0));
                }
                else
                {
                    audioSource.PlayOneShot(clip[1]);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(FMain);
                    StartCoroutine(fade(3));
                }
                break;
            case 3:
                audioSource.PlayOneShot(clip[0]);
                Slider.GetComponent<Slider>().value = 50f;
                imageControls.sprite = imageKeyboard;
                break;
            case 4:
                audioSource.PlayOneShot(clip[0]);
                break;
            case 5:
                audioSource.PlayOneShot(clip[1]);
                StartCoroutine(fade(1));
                break;
        }
    }
    
    public void changeControlsImage(int i)
    {
        if (i == 0)
            imageControls.sprite = imageKeyboard;

        if (i == 1)
            imageControls.sprite = imageGamepad;

        print(i);
    }

    public void SetVolume()
    {
        if(Slider.GetComponent<Slider>().value%10f==0)
            audioSource.PlayOneShot(clip[0]);
        
        audioSource.volume = 1 - Slider.GetComponent<Slider>().value/100;
    }

    IEnumerator fade(int j)
    {
        for(int i = 0; i<110; i++)
        {
            yield return new WaitForSeconds(.01f);
            transicion.GetComponent<Image>().color = new Color(transicion.GetComponent<Image>().color.r,
                transicion.GetComponent<Image>().color.g,
                transicion.GetComponent<Image>().color.b,
                transicion.GetComponent<Image>().color.a + 0.01f);
        }
        switch(j)
        {
            case 0:
                Fondo1.SetActive(false);
                Fondo2.SetActive(true);
                transicion.GetComponent<Image>().color = new Color(transicion.GetComponent<Image>().color.r,
                transicion.GetComponent<Image>().color.g,
                transicion.GetComponent<Image>().color.b,
                0);
                break;
            case 1:
                //partida nueva
                SceneManager.LoadScene(6);
                break;
            case 2:
                //cargar partida
                break;
            case 3:
                Fondo1.SetActive(true);
                Fondo2.SetActive(false);
                transicion.GetComponent<Image>().color = new Color(transicion.GetComponent<Image>().color.r,
                transicion.GetComponent<Image>().color.g,
                transicion.GetComponent<Image>().color.b,
                0);
                break;
        }
        
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class GridInventoryManager : MonoBehaviour
{
    public Party party;
    public GameEventItem geiRemove;
    public GameEventItem geiAdd;
    public PlayerInput player;
    private GameObject equipmentGrid;
    private bool onEquip = false;
    private GameObject consumablesGrid;
    private int pointerPos = 0;
    private GameObject pointer;
    private GameObject submenuPointer;
    int submenuPointerPos = 0;
    private GameObject consumablesMenu;
    private GameObject equipmentMenu;
    private GameObject infoTxt;
    private void Awake()
    {
        submenuPointer = GameObject.Find("PointerSubmenu");
        pointer = GameObject.Find("Pointer");
        equipmentGrid = GameObject.Find("GridEquipment");
        consumablesGrid = GameObject.Find("GridConsumables");
        consumablesMenu = GameObject.Find("ConsumablesMen�");
        equipmentMenu = GameObject.Find("EquipmentMen�");
        infoTxt = GameObject.Find("infoPanel");

    }
    private void OnEnable()
    {
        player.actions["Move"].performed += movePointer;
        player.actions["Move"].performed += movesubPointer;
        player.actions["btnAccion"].performed += selectItem;
        infoTxt.SetActive(false);
        submenuPointer.SetActive(false);
        consumablesMenu.SetActive(false);
        equipmentMenu.SetActive(false); 
        pointer.SetActive(false);
        print("HE ABIERTO EL MEN� DE INVENTARIO");
        reloadMenu();
    }
    private void reloadMenu()
    {
        for (int i = 0; i < equipmentGrid.transform.childCount; i++)
        {
            Destroy(equipmentGrid.transform.GetChild(0).gameObject);
        }
        for (int i = 0; i < consumablesGrid.transform.childCount; i++)
        {
            Destroy(consumablesGrid.transform.GetChild(0).gameObject);
        }
        foreach (RPGItem item in party.inventory.getInventory())
        {
            print(item.name);
            GameObject cell = new GameObject();
            cell.AddComponent<Image>();
            cell.GetComponent<Image>().sprite = item.sprite;
            cell.transform.name = item.name;
            print(cell.transform.name);
            cell.transform.parent = (item is Equipable) ? equipmentGrid.gameObject.transform : consumablesGrid.gameObject.transform;
        }
        submenuPointer.SetActive(false);
        consumablesMenu.SetActive(false);
        equipmentMenu.SetActive(false);

    }
    private void OnDisable()
    {
        reloadMenu();
        onEquip = false;
        player.actions["Move"].performed -= movePointer;
        player.actions["Move"].performed -= movesubPointer;
        player.actions["btnAccion"].performed -= selectItem;
    }

    public void enablePointer(int index)
    {
        pointerPos = 0;
        switch (index)
        {
            case 1:
                pointerOnEquip();
                break;
            case 2:
                pointerOnConsum();
                break;
            default:
                break;
        }
    }

    private void pointerOnConsum()
    {
        if (consumablesGrid.transform.childCount == 0) pointer.SetActive(false);
        else
        {
            onEquip = false;
            pointer.transform.position = consumablesGrid.transform.GetChild(0).position;
            pointer.SetActive(true);
        }

    }

    private void pointerOnEquip()
    {
        if (equipmentGrid.transform.childCount == 0) pointer.SetActive(false);
        else
        {
            onEquip = true;
            pointer.transform.position = equipmentGrid.transform.GetChild(0).position;
            pointer.SetActive(true);
        }
    }
    public void movesubPointer(InputAction.CallbackContext ctxt)
    {
        if(submenuPointer.activeSelf)
        {
            if (ctxt.performed)
            {
                Vector2 value = ctxt.ReadValue<Vector2>();
                if (value.y < 0)
                {
                    submenuPointerPos++; 
                }
                else if (value.y > 0)
                {
                    submenuPointerPos--;
                }
                submenuPointerPos = (submenuPointerPos < 0) ? 0 : submenuPointerPos;
                submenuPointerPos = (submenuPointerPos > consumablesMenu.transform.GetChild(0).transform.childCount - 1) ? consumablesMenu.transform.GetChild(0).transform.childCount - 1 : submenuPointerPos;
                if(onEquip)
                {
                    submenuPointer.transform.position = equipmentMenu.transform.GetChild(0).transform.GetChild(submenuPointerPos).position;
                }
                else
                {
                    submenuPointer.transform.position = consumablesMenu.transform.GetChild(0).transform.GetChild(submenuPointerPos).position;
                }
                


            }
        }
    }

    public void movePointer(InputAction.CallbackContext ctxt)
    {
        if (pointer.activeSelf)
        {
            if (ctxt.performed)
            {
                Vector2 value = ctxt.ReadValue<Vector2>();
                if (value.x < 0)
                {
                    pointerPos--;
                }
                else if (value.x > 0)
                {
                    pointerPos++;
                }
                else if(value.y < 0)
                {
                    pointerPos += party.inventory.getConsumables().Count / 2;
                }
                else if(value.y > 0)
                {
                    pointerPos -= party.inventory.MAX_INVENTORY / 2;
                }
                pointerPos = (pointerPos < 0) ? 0 : pointerPos;
                if (onEquip)
                {
                    pointerPos = (pointerPos > equipmentGrid.transform.childCount - 1) ? equipmentGrid.transform.childCount - 1 : pointerPos;
                    pointer.transform.position = equipmentGrid.transform.GetChild(pointerPos).position;
                }
                else
                {
                    pointerPos = (pointerPos > consumablesGrid.transform.childCount - 1) ? consumablesGrid.transform.childCount - 1 : pointerPos;
                    pointer.transform.position = consumablesGrid.transform.GetChild(pointerPos).position;
                }

            }
        }
    }

    public void selectItem(InputAction.CallbackContext ctxt)
    {
        if (ctxt.performed)
        {
            if (submenuPointer.activeSelf)
            {
                List<RPGItem> inventoryToIterate;
                if (consumablesMenu.activeSelf)
                {
                    inventoryToIterate = new List<RPGItem>(party.inventory.getConsumables());
                }
                else
                {
                    inventoryToIterate = new List<RPGItem>(party.inventory.getEquipment());
                }
                switch (submenuPointerPos)
                {
                    case 0:
                        //Info
                        infoTxt.SetActive(true);
                        infoTxt.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventoryToIterate[pointerPos].type.ToString() +
                            Environment.NewLine +
                            inventoryToIterate[pointerPos].description;
                        infoTxt.transform.position = new Vector2(submenuPointer.transform.position.x + 78, submenuPointer.transform.position.y - 34);
                        break;
                    case 1:
                        //Use/Equip
                        if (inventoryToIterate[pointerPos] is Weapon)
                        {
                            Weapon weapon = (Weapon)inventoryToIterate[pointerPos];
                            weapon.equip(party.party[0]);

                        }
                        else if (inventoryToIterate[pointerPos] is Potion)
                        {
                            Potion potionToUse = (Potion)inventoryToIterate[pointerPos];
                            potionToUse.consume(party.party[0]);
                        }
                        reloadMenu();
                        pointer.SetActive(true);
                        break;
                    case 2:
                        //Drop
                        geiRemove.Raise(inventoryToIterate[pointerPos]);
                        reloadMenu();
                        break;
                    default:
                        break;
                }
            }
            else if (pointer.activeSelf)
            {
                pointer.SetActive(false);
                GameObject inventoryToIterate = (onEquip) ? equipmentGrid : consumablesGrid;
                GameObject menuToUse = (onEquip) ? equipmentMenu : consumablesMenu;
                menuToUse.SetActive(true);
                menuToUse.transform.position = inventoryToIterate.transform.GetChild(pointerPos).position;
                enableSubmenu(menuToUse);
            }
        }
    }

    public void enableSubmenu(GameObject menu)
    {
        submenuPointer.SetActive(true);
        submenuPointer.transform.position = menu.transform.GetChild(0).transform.GetChild(submenuPointerPos).position;   
    }
}

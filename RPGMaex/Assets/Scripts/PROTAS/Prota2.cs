using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prota2 : MonoBehaviour
{
    public Characters prota2;
    static GameObject instantiateProta = null;
    void Awake()
    {
        if (instantiateProta == null)
        {
            instantiateProta = this.gameObject;
            DontDestroyOnLoad(instantiateProta);
        }
        else
        {
            Destroy(this.gameObject);
        }

        prota2.m_ATTK = 12;
        prota2.m_DEF = 8;
        prota2.m_SPD = 15;
        prota2.m_MaxHP = 90;
        prota2.m_CurrentHP = 75;
        prota2.m_MaxPP = 56;
        prota2.m_CurrentPP = 45;
        prota2.m_lvl = 2;
        prota2.m_exp = 12;
    }
}

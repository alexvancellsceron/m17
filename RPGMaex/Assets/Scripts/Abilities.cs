using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Abilities : ScriptableObject
{
    public string m_name;
    public string m_description;
    public Nature m_type;
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEditor.Animations;
using UnityEngine;

public abstract class Characters : ScriptableObject
{
    public int m_lvl;
    public string m_name;
    public float m_exp;
    public float m_MaxHP;
    public float m_CurrentHP;
    public float m_ATTK;
    public float m_DEF;
    public float m_MaxPP;
    public float m_CurrentPP;
    public float m_SPD;
    public List<Abilities> m_Abilities;
    public Sprite m_sprite;
    public AnimatorController mAnimatorController;

    public float getMaxHP()
    {
        return this.m_MaxHP;
    }
    public void setMaxHP(float value)
    {
        this.m_MaxHP = value;
    }
    public float getCurrentHP()
    {
        return this.m_CurrentHP;
    }
    public void setCurrentHP(float value)
    {
        this.m_CurrentHP = value;
    }
    public float getATTK()
    {
        return this.m_ATTK;
    }
    public void setATTK(float value)
    {
        this.m_ATTK = value;
    }
    public float getDEF()
    {
        return this.m_DEF;
    }
    public void setDEF(float value)
    {
        this.m_DEF = value;
    }
    public float getMaxPP()
    {
        return this.m_MaxPP;
    }
    public void setMaxPP(float value)
    {
        this.m_MaxPP = value;
    }
    public float getCurrentPP()
    {
        return this.m_CurrentPP;
    }
    public void setCurrentPP(float value)
    {
        this.m_CurrentPP = value;
    }
    public float getSPD()
    {
        return this.m_SPD;
    }
    public void setSPD(float value)
    {
        this.m_SPD = value;
    }
    public float getStat(Stats stat)
    {
        float value;
        switch(stat)
        {
            case Stats.SPD:
                value = getSPD();
                break;
            case Stats.HP:
                value = getCurrentHP();
                break;
            case Stats.ATTK:
                value = getATTK();
                break;
            case Stats.DEF:
                value = getDEF();
                break;
            case Stats.PP:
                value = getCurrentPP();
                break;
            default:
                value = 0;
                break;
        }
        return value;
    }

    public void setStat(float value, Stats stat)
    {
        switch (stat)
        {
            case Stats.SPD:
                setSPD(getStat(stat) + value);
                break;
            case Stats.HP:
                setCurrentHP(getStat(stat) + value);
                break;
            case Stats.ATTK:
                setATTK(getStat(stat) + value);
                break;
            case Stats.DEF:
                setDEF(getStat(stat) + value);
                break;
            case Stats.PP:
                setCurrentPP(getStat(stat) + value);
                break;
        }
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class Enemies : Characters
{

    public GameEventItem gei;
    [Serializable]
    public struct Loot
    {
        public RPGItem item;
        public float probability;
    }
    public Loot[] loot;

    public void drop()
    {
        float random = Random.Range(0, 1f);
        Debug.Log(random);
        foreach(Loot item in loot)
        {
            Debug.Log(item.probability);
            if(random<item.probability)
            {
                Debug.Log("HAGO EL RAISE");
                gei.Raise(item.item);
                //Add the item to the inventory (InventoryStackManager.AddItem(item);)
            }
        }
    }

}

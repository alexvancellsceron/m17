// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerItem : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventItem Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<RPGItem> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(RPGItem item)
    {

        Response.Invoke(item);
    }
}


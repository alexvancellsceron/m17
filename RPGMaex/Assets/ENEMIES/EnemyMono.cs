using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Animations;
using UnityEngine;

public class EnemyMono : MonoBehaviour
{
    public List<Enemies> listEnemies/* = new List<Enemies>()*/;
    int i, selected;

    public delegate void pasarEnemySelected(Enemies e);
    public event pasarEnemySelected mPasarEnemySelected;

    // Start is called before the first frame update
    void Start()
    {
        i = listEnemies.Count;
        selected = Random.Range(0, i);
        this.GetComponent<Animator>().runtimeAnimatorController = listEnemies[selected].mAnimatorController;
        listEnemies[selected].setCurrentHP(listEnemies[selected].getMaxHP());
        
        StartCoroutine(esperar());    
    }

    IEnumerator esperar()
    {
        yield return new WaitForSeconds(1);
        mPasarEnemySelected += fake;
        mPasarEnemySelected.Invoke(listEnemies[selected]);

    }

    public void fake(Enemies e)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (listEnemies[selected].getCurrentHP()<1)
        {
            this.listEnemies[selected].drop();
            Destroy(this.gameObject);
        }
            
    }
}

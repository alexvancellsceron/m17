using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Xml;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.VFX;
using Random = UnityEngine.Random;

public class GameManagerCombate : MonoBehaviour
{
    [Serialize]
    public GameObject enemy;
    List<Enemies> listEnemyMono;
    GameObject[] recibirEnemySelected;
    GameObject instantiateEnemy;
    int numEnemies;
    Enemies objetivoEnemigo1, objetivoEnemigo2;
    bool can = false;
    bool turnoDos = false;
    string accion1 = null;
    string accion2 = null;
    int numMuertosFinombate;
    Dictionary<string, float> ordenSteps = new Dictionary<string, float>();

    public PlayableCharacters prota1, prota2;
    List<GameObject> listEnemies = new List<GameObject>();

    public delegate void pasarNumEnemies(int i);
    public event pasarNumEnemies mNum;

    static GameObject GMC = null;

    void Awake()
    {
        if (GMC == null)
        {
            GMC = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(GMC);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    EnemyMono em;

    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        if (s.name == "Combate")
        {
            if(can)
            {
                listEnemies.Clear();
                numEnemies = Random.Range(1, 4);
                numMuertosFinombate = numEnemies;
                mNum?.Invoke(numEnemies);
                instanciarEnemies(numEnemies);
                turnoDos = false;
            }
            else
            {
                numMuertosFinombate = 3;
                instanciarEnemies(3);
                can = true;
            }
            
            listEnemyMono = new List<Enemies>();
            /*
            recibirEnemySelected1 = GameObject.Find("PreFabEnemie(Clone)");
            recibirEnemySelected1.GetComponent<EnemyMono>().mPasarEnemySelected += funcionRecibirEnemy;
            */
            recibirEnemySelected = GameObject.FindGameObjectsWithTag("Enemy");
            for(int i = 0; i<recibirEnemySelected.Count(); i++)
            {
                recibirEnemySelected[i].GetComponent<EnemyMono>().mPasarEnemySelected += funcionRecibirEnemy;
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        

    }

    public void funcionRecibirEnemy(Enemies e)
    {
        e.m_CurrentHP = e.m_MaxHP;
        print(e.m_CurrentHP+" <-- current hp enemy");   
        listEnemyMono.Add(e);
    }

    private void instanciarEnemies(int i)
    {
        
        for (int j = 0; j < i; j++)
        {
            instantiateEnemy = Instantiate(enemy);
            instantiateEnemy.transform.position = new Vector2(-1-j, 5.5f-(j*2+2));
            instantiateEnemy.GetComponent<Animator>().runtimeAnimatorController = enemy.GetComponent<Animator>().runtimeAnimatorController;
            listEnemies.Add(instantiateEnemy);
        }
    }

    public void guardarAccion(int v)
    {
        switch (v)
        {
            case 0:
                if(!turnoDos)
                {
                    accion1 = "ataqueFisico";
                }
                else
                {
                    accion2 = "ataqueFisico";
                }
                
                break;
            case 1:
                
                break;
            case 2:
                if (!turnoDos)
                {
                    accion1 = "ataqueParty";
                }
                else
                {
                    accion2 = "ataqueParty";
                }
                break;
            case 3:
                
                break;
        }
    }

    public void guardarObjetivo(int v)
    {
        switch (v)
        {
            case 0:
                if(!turnoDos)
                {
                    objetivoEnemigo1 = listEnemyMono[0];
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemyMono[0];
                    turnoDos = false;
                    goSteps();
                }
                
                break;
            case 1:
                if (!turnoDos)
                {
                    objetivoEnemigo1 = listEnemyMono[1];
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemyMono[1];
                    turnoDos = false;
                    goSteps();
                }

                break;
            case 2:
                if (!turnoDos)
                {
                    objetivoEnemigo1 = listEnemyMono[2];
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemyMono[2];
                    turnoDos = false;
                    goSteps();
                }
                break;
        }
    }

    public void goSteps()
    {
        ordenSteps.Clear();
        
        for(int i = 0; i<listEnemyMono.Count; i++)
        {
            if(listEnemyMono[i].m_CurrentHP>0)
                ordenSteps.Add(listEnemyMono[i].m_name+ i, listEnemyMono[i].m_SPD);
        }
        
        
        ordenSteps.Add(prota1.m_name, prota1.m_SPD);
        ordenSteps.Add(prota2.m_name, prota2.m_SPD);
        
        ordenSteps = ordenSteps.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

        foreach (KeyValuePair<string, float> entry in ordenSteps)
        {
            //print("LISTA: " + entry.Key);
            if (entry.Key == "Lilith")
            {
                realizarAtaqueProta(objetivoEnemigo2, accion2, prota2);
            }
            else if(entry.Key == prota1.m_name)
            {
                realizarAtaqueProta(objetivoEnemigo1, accion1, prota1);
            }
            else
            {
                int r = Random.Range(0, 2);
                if(r==0)
                {
                    for (int i = 0; i < listEnemyMono.Count; i++)
                    {
                        if(entry.Key == (listEnemyMono[i].m_name+i))
                        {
                            if(listEnemyMono[i].m_CurrentHP>1)
                                realizarAtaqueEnemy(listEnemyMono[i], prota1);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < listEnemyMono.Count; i++)
                    {
                        if (entry.Key == (listEnemyMono[i].m_name + i))
                        {
                            if (listEnemyMono[i].m_CurrentHP > 1)
                                realizarAtaqueEnemy(listEnemyMono[i], prota2);
                        }
                    }
                }
            }
        }
        if(numMuertosFinombate<1)
        {
            for(int i = 0;  i<listEnemyMono.Count; i++)
            {
                float exp1 = prota1.m_exp+ listEnemyMono[i].m_exp;
                prota1.setCurrentExp(exp1);

                float exp2 = prota2.m_exp + listEnemyMono[i].m_exp;
                prota2.setCurrentExp(exp2);
            }
            
            SceneManager.LoadScene(5);
        }
        acabarTurno();

    }

    public void realizarAtaqueProta(Enemies e, string a, PlayableCharacters p)
    {
        switch(a)
        {
            case "ataqueFisico":
                e.m_CurrentHP = (float)Math.Floor(e.m_CurrentHP - (((p.m_ATTK / e.m_DEF) + 1) * 5));
                break;
            case "ataqueParty":
                e.m_CurrentHP = (float)Math.Floor(e.m_CurrentHP - ((((prota1.m_ATTK + prota2.m_ATTK) / e.m_DEF) + 1) * 5));
                break;
        }

        print(p.m_name + " ha realizado un ataque sobre " + e.m_name + " y le quedan " + e.m_CurrentHP + " puntos de vida");
        if(e.m_CurrentHP<1)
        {
            numMuertosFinombate--;
            print("enemigos vivos " + numMuertosFinombate);
            int index = listEnemyMono.IndexOf(e);
            print(index+" <-- index");
            listEnemies[index].SetActive(false);
            print(e.m_name + " ha muerto");
        }
    }

    public void realizarAtaqueEnemy(Enemies e, PlayableCharacters p)
    {
        print("enemigo " + e.m_name + " realiza ataque sobre "+p.m_name);

        p.m_CurrentHP = (float) Math.Floor(p.m_CurrentHP - (((e.m_ATTK/p.m_DEF)+1)*5) );
        if(p.m_CurrentHP<1)
        {
            print(p.m_name + " ha muerto");
        }
    }

    public void acabarTurno()
    {
        turnoDos = false;
    }


}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platform_script : MonoBehaviour
{
    public RPGItem key;
    public GameEventItem add;
    public QuestSO key_quest;
    private bool hasKey = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(hasKey)
        {
            add.Raise(key);
            hasKey = false;
            key_quest.collected = true;
        }

    }
}

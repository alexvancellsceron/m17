using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : PadreDanar
{
    Rigidbody2D rb;
    float p, t;
    public GameEvent GEO;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        this.ataque = 3;
        this.GetComponent<CircleCollider2D>().isTrigger = false;
        StartCoroutine(controlMuerte());
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        print("T " + collision.transform.tag);
        if (collision.transform.tag == "Player" || collision.transform.tag == "Saco")
        {
            float danoVida;
            p = 3;
            t = 0;
            if (collision.gameObject.GetComponent<Rigidbody2D>().transform.position.x < rb.transform.position.x)
            {
                danoVida = collision.gameObject.GetComponent<PadreDanar>().Danar(p, this.ataque, true, t);
            }
            else
            {
                danoVida = collision.gameObject.GetComponent<PadreDanar>().Danar(p, this.ataque, false, t);
            }
            GEO.Raise(danoVida, !collision.gameObject.GetComponent<PadreDanar>().isPLayerOne);
            
        }
    }


    IEnumerator controlMuerte()
    {
        yield return new WaitForSeconds(3.7f);
        this.GetComponent<CircleCollider2D>().isTrigger = true;
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}

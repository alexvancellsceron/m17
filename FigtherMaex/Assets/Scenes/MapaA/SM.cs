using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class SM : PadreDanar 
{
    /* defining states --------------------------------------------------------------------------------------------- */
    //vacilada -- Q
    bool v;

    //salto normal -- W
    bool s1;

    //salto doble -- WW

    //salto triple -- WWW
    bool s3;

    //derecha -- D

    //correr derecha -- DD

    //embestida derecha -- [if(Status.corriendoderecha] 4
    bool ed;

    //izquierda -- A

    //correr izquierda -- AA

    //embestida izquierda -- [if(Status.corriendoderecha] 4
    bool ei;

    //abajo/agacharse -- S
    bool aa;

    // ataque normal 1 -- 4
    bool an1;

    // ataque normal 2 -- 44
    bool an2;

    //ataque normal 3 -- 444
    bool an3;

    //ataque aereo 1 -- [if (s1||s2)] 4
    bool aan;

    //ataque aereo 2 -- [if (s1||s2)] 5
    bool aas;

    //atque especial 1 -- 5
    bool as1;

    //ataque especial 2 -- 45
    bool ae2;

    //ataque especial definitivo -- Q44455
    bool ad;

    //ataque proyectil -- D5/A5
    bool ap;

    //protegerse -- 6
    bool c;

    //idle
    bool idle;

    /* Final definig states ------------------------------------------------------------------------------------------------------------------------ */

    public GameObject prefabProyectil;
    private GameObject proyectilInstantiate;

    private Status state;
    private StatusJump statejump;
    Coroutine CorrutinaGuardada;
    Coroutine CorrutinaGuardadaJump;
    Rigidbody2D rb;
    float vel = 5f;
    bool salto;

    public Animator animatior;

    bool izquierdaDir;
    bool derechaDir;

    float childPos;

    public SOMenu SOMenu;

    public GameEvent GEO;
    private List<bool> boleanos = new List<bool>();

    float t;
    float p;

    // Start is called before the first frame update
    void Start()
    {
        this.ataque = 10;
        this.defensa = 5;
        this.vida = 0;
        rb = this.GetComponent<Rigidbody2D>();
        state = Status.NONE;
        statejump = StatusJump.NONE;
        derechaDir = true;
        izquierdaDir = false;
        float parentPos = this.transform.parent.GetComponent<Rigidbody2D>().position.x;
        childPos = parentPos - rb.position.x;
        //SOMenu.transformCh1 = rb.transform;
    }



    void Update()
    {
        if (idle)
        {
            animatior.Play("idle");
        }
        this.transform.parent.GetComponent<Rigidbody2D>().velocity = rb.velocity;

        if (derechaDir)
            this.transform.parent.GetComponent<Rigidbody2D>().position = new Vector2(rb.position.x + childPos, rb.position.y);

        if (izquierdaDir)
            this.transform.parent.GetComponent<Rigidbody2D>().position = new Vector2(rb.position.x - childPos, rb.position.y);
        
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Suelo")
        {
            if (s1 || aa)
            {
                animatior.Play("idle");
            }
            s1 = false;
            s3 = false;
            salto = false;
            statejump = StatusJump.NONE;
            if(aa)
            {
                aa = false;
                rb.AddForce(new Vector2(0, 1000));
            }
        }
        aa = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.transform.tag == "Player" || collision.transform.tag == "Saco")
        {
            float danoVida;
            
            if (collision.GetComponent<Rigidbody2D>().transform.position.x<rb.transform.position.x)
            {
                danoVida = collision.GetComponent<PadreDanar>().Danar(p, this.ataque, true, t);
            }
            else
            {
                danoVida = collision.GetComponent<PadreDanar>().Danar(p, this.ataque, false, t);
            }
            GEO.Raise(danoVida, this.isPLayerOne);
            p = 0;
            t = 0;
        }
    }

    public void Jump(InputAction.CallbackContext context)
    {

        switch (statejump)
        {
            case StatusJump.saltoNormal:
                if (context.performed)
                {
                    t = 0;
                    p = 2;
                    idle = false;
                    statejump = StatusJump.saltoDoble;
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    rb.AddForce(new Vector2(rb.velocity.x, 1500));
                    animatior.Play("jump2");
                    StopCoroutine(CorrutinaGuardadaJump);
                    StartCoroutine(cJump(0.833f));
                }
                break;
            case StatusJump.saltoDoble:
                if (context.performed)
                {
                    idle = false;
                    statejump = StatusJump.NONE;
                    s3 = true;
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    rb.AddForce(new Vector2(rb.velocity.x, 3000));
                    animatior.Play("jump3");
                    StopCoroutine(CorrutinaGuardadaJump);
                    StartCoroutine(cJump(0.833f));
                }
                break;
            default:
                if (!salto && context.performed)
                {
                    idle = false;
                    statejump = StatusJump.saltoNormal;
                    s1 = true;
                    salto = true;
                    rb.AddForce(new Vector2(rb.velocity.x, 2000));
                    animatior.Play("jump1");
                    CorrutinaGuardadaJump = StartCoroutine(cJump(0.833f));
                }

                break;
        }
    }

    public void Vacilada(InputAction.CallbackContext context)
    {
        switch (state)
        {
            default:
                if (!v && !s1 && context.performed)
                {
                    idle = false;
                    state = Status.vacilada;
                    v = true;
                    if (CorrutinaGuardada != null)
                    {
                        StopCoroutine(CorrutinaGuardada);
                    }
                    CorrutinaGuardada = StartCoroutine(timeToBreak(1f));
                    animatior.Play("vacilada");
                }
                break;
        }

    }

    public void Izquierda(InputAction.CallbackContext context)
    {
        idle = false;
        switch (state)
        {
            case Status.izquierda:
                if (!s1 && context.performed)
                {
                    state = Status.corriendoIzq;
                    rb.velocity = new Vector2(-vel * 2.5f, rb.velocity.y);
                    StopCoroutine(CorrutinaGuardada);
                    animatior.Play("correrDer");
                    CorrutinaGuardada = StartCoroutine(timeToBreak(3f));

                }
                if (context.canceled)
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                    idle = true;
                }
                break;
            default:
                if (context.performed)
                {
                    state = Status.izquierda;
                    derechaDir = false;

                    if (!izquierdaDir)
                    {
                        izquierdaDir = true;
                        this.transform.parent.transform.localScale *= new Vector2(-1, 1);
                    }
                    rb.velocity = new Vector2(-vel, rb.velocity.y);
                    if (CorrutinaGuardada != null)
                    {
                        StopCoroutine(CorrutinaGuardada);
                    }
                    CorrutinaGuardada = StartCoroutine(timeToBreak(0.3f));
                }
                if (context.canceled)
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                    idle = true;
                }
                break;
        }

    }

    public void Derecha(InputAction.CallbackContext context)
    {
        idle = false;
        switch (state)
        {
            case Status.derecha:
                if (!s1 && context.performed)
                {
                    animatior.Play("correrDer");
                    state = Status.corriendoDer;
                    rb.velocity = new Vector2(vel * 2.5f, rb.velocity.y);
                    StopCoroutine(CorrutinaGuardada);
                    CorrutinaGuardada = StartCoroutine(timeToBreak(3f));
                }
                if (context.canceled)
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                    idle = true;
                }
                break;
            default:
                if (context.performed)
                {
                    state = Status.derecha;
                    izquierdaDir = false;

                    if (!derechaDir)
                    {
                        derechaDir = true;
                        this.transform.parent.transform.localScale *= new Vector2(-1, 1);
                    }
                    rb.velocity = new Vector2(vel, rb.velocity.y);
                    if (CorrutinaGuardada != null)
                    {
                        StopCoroutine(CorrutinaGuardada);
                    }
                    CorrutinaGuardada = StartCoroutine(timeToBreak(0.3f));
                }
                if (context.canceled)
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                    idle = true;
                }
                break;
        }
    }

    public void AtaqueNormal(InputAction.CallbackContext context)
    {

        idle = false;
        if (!s1 && !s3)
        {
            switch (state)
            {
                case Status.corriendoDer:
                case Status.corriendoIzq:
                    if (context.performed)
                    {
                        if (state == Status.corriendoDer)
                        {
                            p = 3;
                            t = 0;
                            rb.velocity = new Vector2(0, rb.velocity.y);
                            animatior.Play("embestida");
                            StopCoroutine(CorrutinaGuardada);

                        }
                        if (state == Status.corriendoIzq)
                        {
                            p = 3;
                            t = 0;
                            rb.velocity = new Vector2(0, rb.velocity.y);
                            animatior.Play("embestida");
                            StopCoroutine(CorrutinaGuardada);

                        }
                        state = Status.NONE;
                    }
                    break;
                case Status.vacilada:
                    if (context.performed)
                    {
                        state = Status.Q4;
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }
                    break;
                case Status.Q4:
                    if (context.performed)
                    {
                        state = Status.Q44;
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }
                    break;
                case Status.Q44:
                    if (context.performed)
                    {
                        state = Status.Q444;
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }
                    break;
                case Status.ataqueNormal1:
                    if (context.performed)
                    {
                        p = 2;
                        t = 0.5f;
                        state = Status.ataqueNormal2;
                        animatior.Play("ataqueNormal2");
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }
                    break;
                case Status.ataqueNormal2:
                    if (context.performed)
                    {
                        p = 3;
                        t = 0;
                        state = Status.NONE;
                        animatior.Play("ataqueNormal3");
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.583f));
                    }
                    break;
                default:
                    if (context.performed)
                    {
                        p = 2;
                        t = 0.5f;
                        state = Status.ataqueNormal1;
                        animatior.Play("ataqueNormal1");
                        if (CorrutinaGuardada != null)
                        {
                            StopCoroutine(CorrutinaGuardada);
                        }
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }

                    break;
            }

        }
        else if (s1 && !s3)
        {
            if (context.performed)
            {
                p = 1;
                t = 0;
                state = Status.NONE;
                animatior.Play("ataqueAereoNormal");
                if (CorrutinaGuardada != null)
                {
                    StopCoroutine(CorrutinaGuardada);
                }
                CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
            }
        }

    }

    public void AtaqueEspecial(InputAction.CallbackContext context)
    {
        if (!s1 && !s3)
        {
            idle = false;
            switch (state)
            {
                case Status.derecha:
                case Status.izquierda:
                    if (context.performed)
                    {
                        p = 3;
                        t = 0;
                        animatior.Play("proyectil");
                        StartCoroutine(esperaProyectil(state));
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5883f));
                    }
                    break;
                case Status.Q444:
                    if (context.performed)
                    {
                        state = Status.Q4445;
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.5f));
                    }
                    break;
                case Status.Q4445:
                    if (context.performed)
                    {
                        p = 5;
                        t = 0;
                        state = Status.NONE;
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(2f));
                        animatior.Play("definitivo");
                    }
                    break;
                case Status.ataqueNormal1:
                    if (context.performed)
                    {
                        p = 2;
                        t = 0;
                        state = Status.NONE;
                        animatior.Play("ataqueEspecial2");
                        StopCoroutine(CorrutinaGuardada);
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.667f));
                    }
                    break;
                default:
                    if (context.performed)
                    {
                        p = 2;
                        t = 0;
                        state = Status.NONE;
                        animatior.Play("ataqueEspecial1");
                        if (CorrutinaGuardada != null)
                        {
                            StopCoroutine(CorrutinaGuardada);
                        }
                        CorrutinaGuardada = StartCoroutine(timeToBreak(0.667f));
                    }

                    break;
            }
        }
        else if (s1 && !s3)
        {
            if (context.performed)
            {
                p = 1;
                t = 0;
                idle = false;
                state = Status.NONE;
                animatior.Play("ataqueAereoEspecial");
                if (CorrutinaGuardada != null)
                {
                    StopCoroutine(CorrutinaGuardada);
                }
                CorrutinaGuardada = StartCoroutine(timeToBreak(0.667f));
            }
        }


    }

    public void Protegerse(InputAction.CallbackContext context)
    {
        if (!c && !s1 && !s3 && context.performed)
        {
            idle = false;
            state = Status.NONE;
            c = true;
            animatior.Play("cubrirse");
            this.transform.tag = "Untagged";
            if (CorrutinaGuardada != null)
            {
                StopCoroutine(CorrutinaGuardada);
            }
            StartCoroutine(ponerFalseCubrirse());
            CorrutinaGuardada = StartCoroutine(timeToBreak(3));
        }
        if (context.canceled)
        {
            idle = true;
            c = false;
            this.transform.tag = "Player";
            StartCoroutine(timeToBreak(3));
        }

    }

    public void AtaqueAbajo(InputAction.CallbackContext context)
    {
        if (s1 && !s3 && context.performed)
        {
            p = 3;
            t = 0;
            idle = false;
            aa = true;
            state = Status.NONE;
            animatior.Play("ataqueAbajo");
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, -8000));
            if (CorrutinaGuardada != null)
            {
                StopCoroutine(CorrutinaGuardada);
            }
            CorrutinaGuardada = StartCoroutine(timeToBreak(2));
        }
    }

    IEnumerator timeToBreak(float x)
    {
        yield return new WaitForSeconds(x);
        state = Status.NONE;

        v = false;
        idle = true;
        this.transform.tag = "Player";
    }

    IEnumerator ponerFalseCubrirse()
    {
        yield return new WaitForSeconds(3);
        c = false;
    }

    IEnumerator cJump(float x)
    {
        yield return new WaitForSeconds(x);
        animatior.Play("cayendo");
    }

    IEnumerator esperaProyectil(Status state)
    {
        yield return new WaitForSeconds(0.2f);
        if (state == Status.derecha)
        {
            proyectilInstantiate = Instantiate(prefabProyectil);
            proyectilInstantiate.GetComponent<Rigidbody2D>().AddTorque(-10, ForceMode2D.Impulse);
            proyectilInstantiate.GetComponent<Rigidbody2D>().AddForce(new Vector2(500, -200));
            proyectilInstantiate.GetComponent<Rigidbody2D>().transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 0);
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (state == Status.izquierda)
        {
            proyectilInstantiate = Instantiate(prefabProyectil);
            proyectilInstantiate.GetComponent<Rigidbody2D>().AddTorque(10, ForceMode2D.Impulse);
            proyectilInstantiate.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500, -200));
            proyectilInstantiate.GetComponent<Rigidbody2D>().transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 0);
            proyectilInstantiate.GetComponent<Rigidbody2D>().transform.localScale = new Vector3(
                proyectilInstantiate.GetComponent<Rigidbody2D>().transform.localScale.x * -1,
                proyectilInstantiate.GetComponent<Rigidbody2D>().transform.localScale.y, 0);
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
    }
}

public enum Status
{
    derecha, izquierda, vacilada, ataqueNormal1, ataqueNormal2, corriendoDer, corriendoIzq, Q4, Q44, Q444, Q4445, NONE
}
public enum StatusJump
{
    saltoNormal, saltoDoble, NONE
}


using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.InputSystem;
using UnityEngine;
using UnityEngine.InputSystem;

public class Idle : State
{
    string m_Animation;

    public Idle(FSM fsm, string animation) : base(fsm)
    {
        m_Animation = animation;
    }

    public override void Init()
    {
        m_playerInput["Jump"].performed += jump_action;
        m_playerInput["Simple_Attk"].performed += simple_attk;
        m_playerInput["Shoot"].performed += shoot;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
    }

    

    public override void FixedUpdate()
    {
        if (m_FSM.Owner.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            m_FSM.ChangeState<Fall>();
        }
    }
    private void shoot(InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<Shoot>();
    }
    void simple_attk(InputAction.CallbackContext context)
    {
        if (context.performed) m_FSM.ChangeState<Simple_Attk>();
    } 
    void jump_action(InputAction.CallbackContext context)
    {

        if (context.performed)
        {
            if (!m_FSM.Owner.GetComponent<movement>().m_HasJumped)
            {
                m_FSM.ChangeState<Jump>();
            }
                
        }
        

    }

    public override void Exit()
    {
        m_playerInput["Jump"].performed -= jump_action;
        m_playerInput["Simple_Attk"].performed -= simple_attk;
        m_playerInput["Shoot"].performed -= shoot;
    }

}

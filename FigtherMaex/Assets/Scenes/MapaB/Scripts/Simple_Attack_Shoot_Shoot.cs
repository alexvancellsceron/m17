using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Simple_Attack_Shoot_Shoot : State
{
    string m_Animation;
    float m_StateLength;
    float m_PotensiaAcsoluta;
    float m_DelayTime;
    Coroutine StateLength;

    public Simple_Attack_Shoot_Shoot(FSM fsm, string animation, float stateLength, float potensiaAcsoluta, float delayTime) : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
        m_PotensiaAcsoluta = potensiaAcsoluta;
        m_DelayTime = delayTime;
    }

    public override void Init()
    {
        m_FSM.Owner.GetComponent<movement>().m_DelayTime = m_DelayTime;
        m_FSM.Owner.GetComponent<movement>().m_PotensiaAcsoluta = m_PotensiaAcsoluta;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        StateLength = m_FSM.Owner.GetComponent<movement>().StartCoroutine(stateDuration());
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
    public override void Exit()
    {
        m_FSM.Owner.GetComponent<movement>().StopCoroutine(StateLength);
    }

    IEnumerator stateDuration()
    {
        yield return new WaitForSeconds(m_StateLength);
        m_FSM.ChangeState<Idle>();
    }

}

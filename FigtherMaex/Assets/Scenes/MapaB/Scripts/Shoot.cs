using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : State
{
    string m_Animation;
    bool m_CanShoot;
    float m_StateLength;
    Coroutine StateLength;
    public Shoot(FSM fsm, string animation, float stateLength) : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
    }

    public override void Init()
    {
        m_CanShoot = true;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        if (m_CanShoot)
        {
            shoot_action();
            m_CanShoot = false;
        }
        
        StateLength = m_FSM.Owner.GetComponent<movement>().StartCoroutine(stateDuration());
    }

    private void shoot_action()
    {
        for (int i = 0; i<m_FSM.Owner.GetComponent<movement>().m_Ammo.Count; i++)
        {
            if (!m_FSM.Owner.GetComponent<movement>().m_Ammo[i].activeSelf)
            {
                GameObject bullet = m_FSM.Owner.GetComponent<movement>().m_Ammo[i];
                bullet.transform.position = m_FSM.Owner.transform.position;
                Vector2 bullet_force = (m_FSM.Owner.transform.localScale.x < 0) ? new Vector2(-8, 6) : new Vector2(8, 6);
                bullet.GetComponent<Projectile>().m_Force = bullet_force;
                m_FSM.Owner.GetComponent<movement>().m_Ammo[i].SetActive(true);
                
                break;
            }
        }
    }

    public override void Exit()
    {
        m_FSM.Owner.GetComponent<movement>().StopCoroutine(StateLength);

    }

    IEnumerator stateDuration()
    {
        yield return new WaitForSeconds(m_StateLength);
        m_FSM.ChangeState<Idle>();
    }

}

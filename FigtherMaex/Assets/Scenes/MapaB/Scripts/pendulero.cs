using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pendulero : MonoBehaviour
{
    float m_ChaosCD;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(NoMorePenfuFun());
    }


    // Update is called once per frame
    void Update()
    {
        print(m_ChaosCD);
    }


    IEnumerator APendular()
    {
        this.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_ChaosCD = Random.Range(8.5f,15.5f);
        yield return new WaitForSeconds(m_ChaosCD);
        print("NOMOREFUN");
        StartCoroutine(NoMorePenfuFun());
    }

    IEnumerator NoMorePenfuFun()
    {
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
        this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        float x = (Random.Range(0, 2) == 1) ? 12.63f : -12.63f;
        this.transform.position = new Vector3(x, 10.26f, 0);
        m_ChaosCD = Random.Range(10, 28.5f);
        yield return new WaitForSeconds(m_ChaosCD);
        print("APENDULAR");
        StartCoroutine(APendular());
        
    }
}

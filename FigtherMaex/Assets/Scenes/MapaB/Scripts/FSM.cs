using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace FiniteStateMachine
{
    public class State
    {
        protected FSM m_FSM;
        protected InputActionAsset m_playerInput;
        public State(FSM fsm)
        {
            m_FSM = fsm;
            m_playerInput = m_FSM.Owner.GetComponent<PlayerInput>().actions;
        }

        public virtual void Init() { }
        public virtual void Update() { }
        public virtual void FixedUpdate() { }
        public virtual void Exit() { }
        public virtual void OnTriggerEnter2D(Collider2D collision) 
        {
            if (collision.gameObject.transform.tag == "Player" || collision.gameObject.transform.tag == "Saco")
            {
                bool direction = (collision.gameObject.transform.position.x < m_FSM.Owner.transform.position.x) ? true : false;
                float dano_vida = collision.gameObject.GetComponent<PadreDanar>().Danar(m_FSM.Owner.GetComponent<movement>().m_PotensiaAcsoluta, m_FSM.Owner.GetComponent<movement>().ataque, direction, m_FSM.Owner.GetComponent<movement>().m_DelayTime);
                m_FSM.Owner.GetComponent<movement>().GEO.Raise(dano_vida, m_FSM.Owner.GetComponent<movement>().isPLayerOne);
                MonoBehaviour.print(m_FSM.Owner.GetComponent<PadreDanar>().isPLayerOne);
            }
        }
    }

    public class FSM
    {
        private GameObject m_Owner;
        public GameObject Owner
        {
            get { return m_Owner; }
        }

        private State m_CurrentState = null;
        private List<State> m_States = new List<State>();

        public FSM(GameObject owner)
        {
            m_Owner = owner;
        }

        public void AddState(State state)
        {
            if (!m_States.Contains(state))
                m_States.Add(state);
        }

        public T GetState<T>() where T : State
        {
            foreach (State state in m_States)
                if (state.GetType() == typeof(T))
                    return (T)state;

            return null;
        }

        public bool ChangeState<T>() where T : State
        {
            foreach (State state in m_States)
            {
                if (state.GetType() == typeof(T))
                {
                    m_CurrentState?.Exit();

                    m_CurrentState = state;
                    m_CurrentState.Init();

                    return true;
                }
            }
            return false;
        }

        public void Update()
        {
            m_CurrentState?.Update();
        }
        public void FixedUpdate()
        {
            m_CurrentState?.FixedUpdate();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            m_CurrentState?.OnTriggerEnter2D(collision);
        }
    }

}


using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using State = FiniteStateMachine.State;

public class Walk : State
{
    string m_Animation;
    bool m_VectorUp;

    public Walk(FSM fsm, string animation) : base(fsm)
    {
        m_Animation = animation;
        m_VectorUp = false;
        m_playerInput["Jump"].performed += jump_action;
        m_playerInput["Simple_Attk"].performed += attk_action;
        m_playerInput["Simple_Attk"].performed += up;
        m_playerInput["Move"].performed += moving;
        m_playerInput["Move"].canceled += moving;
            
    }

    public override void Init()
    {
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
       
    }

    public override void FixedUpdate()
    {
        if (m_FSM.Owner.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            m_FSM.ChangeState<Fall>();
        }
    }

    void moving(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
            if (context.ReadValue<Vector2>().y > 0)
            {
                m_VectorUp = true;
            }
        }
        if(context.canceled)
        {
            m_VectorUp=false;
        }
    }
    void up(InputAction.CallbackContext context)
    {
        if (m_VectorUp)
        {
            m_FSM.ChangeState<Up_Air_Attk>();
        }
    }
    void attk_action(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Simple_Attk>();
    }

    void jump_action(InputAction.CallbackContext context)
    {
        if (!m_FSM.Owner.GetComponent<movement>().m_HasJumped)
        {
            m_FSM.ChangeState<Jump>();
        }
    }
}

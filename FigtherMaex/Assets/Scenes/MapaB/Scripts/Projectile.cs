using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PadreDanar
{
    public Vector2 m_Force;
    private float m_DeltaTime;
    private float m_ProjectileDuration = 1.5f;
    float p, t;
    public GameEvent GEO;
    Rigidbody2D m_Rigidbody;
    float danoVida;
    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        this.ataque = 3;
    }

    // Update is called once per frame
    void Update()
    {
        m_DeltaTime += Time.deltaTime;
        if (m_DeltaTime >= 0.1f)
        {
            this.GetComponent<CircleCollider2D>().isTrigger = false;
        }
        if (m_DeltaTime >= m_ProjectileDuration)
        {
            this.gameObject.SetActive(false);
        }
    }
    private void OnEnable()
    {
        m_DeltaTime = 0;
        this.GetComponent<Rigidbody2D>().AddForce(m_Force, ForceMode2D.Impulse);
    }
    private void OnDisable()
    {
        this.GetComponent<CircleCollider2D>().isTrigger = true;
    }
    /*
    public void OnCollisionEnter2D(Collision2D collision)
    {
        this.GetComponent<CircleCollider2D>().isTrigger = true;
    }
    */

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.transform.tag == "Player" || collision.transform.tag == "Saco")
        {
            p = 3;
            t = 0;
            if (collision.GetComponent<Rigidbody2D>().transform.position.x < m_Rigidbody.transform.position.x)
            {
                danoVida = collision.GetComponent<PadreDanar>().Danar(p, this.ataque, true, t);
            }
            else
            {
                danoVida = collision.GetComponent<PadreDanar>().Danar(p, this.ataque, false, t);
            }
            GEO.Raise(danoVida, !collision.gameObject.GetComponent<PadreDanar>().isPLayerOne);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        this.GetComponent<CircleCollider2D>().isTrigger = false;
    }
    /*
    private void OnCollisionExit2D(Collision2D collision)
    {
        this.GetComponent<CircleCollider2D>().isTrigger = false;
    }
    */
}

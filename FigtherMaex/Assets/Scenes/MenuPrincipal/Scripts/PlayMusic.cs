using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    AudioSource audioSource;

    public AudioClip[] clip;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }

    public void PlayMusicFunction(int option)
    {
        switch (option)
        {
            case 1:
                audioSource.PlayOneShot(clip[1]);
                break;
            case 2:
                audioSource.Stop();
                audioSource.PlayOneShot(clip[0]);
                break;
            case 3:
                audioSource.Stop();
                audioSource.PlayOneShot(clip[2]);
                break;
            case 4:
                audioSource.PlayOneShot(clip[3]);
                break;
            case 5:
                audioSource.Stop();
                audioSource.PlayOneShot(clip[3]);
                StartCoroutine(Wait1());
                break;
            case 6:
                audioSource.Stop();
                audioSource.PlayOneShot(clip[3]);
                StartCoroutine(Wait2());
                break;
            case 7:
                audioSource.Stop();
                audioSource.PlayOneShot(clip[3]);
                StartCoroutine(Wait3());
                break;

        }
    }

    IEnumerator Wait1()
    {
        yield return new WaitForSeconds(3);
        audioSource.PlayOneShot(clip[4]);
        audioSource.loop = true;

        /*while (true)
        {
            yield return new WaitForSeconds(179);
            audioSource.PlayOneShot(clip[4]);
        }*/
    }

    IEnumerator Wait2()
    {
        yield return new WaitForSeconds(3);
        audioSource.PlayOneShot(clip[5]);
        audioSource.loop = true;

        /*while (true)
        {
            yield return new WaitForSeconds(237);
            audioSource.PlayOneShot(clip[5]);
        }*/
    }

    IEnumerator Wait3()
    {
        yield return new WaitForSeconds(3);
        audioSource.PlayOneShot(clip[6]);
        audioSource.loop = true;

        /*while (true)
        {
            yield return new WaitForSeconds(237);
            audioSource.PlayOneShot(clip[5]);
        }*/
    }
}

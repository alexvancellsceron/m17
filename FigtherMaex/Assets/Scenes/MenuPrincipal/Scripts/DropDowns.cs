using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class DropDowns : MonoBehaviour
{
    public SOMenu SOMenu;
    public Image ch1, ch2;
    public Sprite gl, gr, cl, cr;

    // Start is called before the first frame update
    void Start()
    {
        SOMenu.player1 = 1;
        SOMenu.player2 = 2;
        SOMenu.practice = false;
    }

    public void cambiarImagenL(int value)
    {
        if (value == 0)
        {
            ch1.sprite = gl;
            SOMenu.player1 = 1;
        }
        else if (value == 1)
        {
            ch1.sprite = cl;
            SOMenu.player1 = 2;
        }
    }

    public void cambiarImagenR(int value)
    {
        if (value == 1)
        {
            ch2.sprite = gr;
            SOMenu.player2 = 1;
        }
        else if (value == 0)
        {
            ch2.sprite = cr;
            SOMenu.player2 = 2;
        }
    }



}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class movimiento : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Saludar(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            print("Hola toni");
        }
    }

    public void Moving(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            print(c.ReadValue<Vector2>());

            this.transform.GetComponent<Rigidbody>().velocity = new Vector3(c.ReadValue<Vector2>().x*4, 0, c.ReadValue<Vector2>().y*4);
        }
    }
}

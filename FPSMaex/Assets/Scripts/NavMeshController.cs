using Cinemachine.Utility;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshController : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform[] transforms;
    int grados;
    public RestarVida rhp;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(path());
    }

    private void Update()
    {
        if (agent.velocity == Vector3.zero)
            agent.transform.Rotate(0, grados, 0);
        else if (agent.velocity != Vector3.zero)
        {
            grados = Random.Range(-1, 2);
            grados /= 10;
        }
            
    }
    public IEnumerator path()
    {
        while (true)
        {
            int i = Random.Range(0, 3);
            yield return new WaitForSeconds(i+1*2);
            setTarget(i);
        }
    }

    public void setTarget(int i)
    {
        if (i == 3)
            agent.speed = 10;
        if(i== -1)
        {
            StartCoroutine(path());
            i = Random.Range(0, 3);
        }
        else
            agent.speed = 6;
        agent.destination = transforms[i].position;
    }

    Coroutine cg;
    public void dispararRaycast(bool b)
    {
        if(b)
            cg = StartCoroutine(rayray());
        else
        {
            if(cg!=null)
                StopCoroutine(cg);
        }

    }

    IEnumerator rayray()
    {
        int rango = 20;
        while (true)
        {
            RaycastHit hitInfo;
            for(int i = -10; i<11; i+=2)
            {
                if (Physics.Raycast(transform.position + new Vector3(0, -.7f, i), transform.forward, out hitInfo, rango))
                {
                    if (hitInfo.transform.tag == "Player")
                    {
                        float dano = rango / hitInfo.distance;
                        hitInfo.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hitInfo.normal * 500 * dano, hitInfo.point);
                        Debug.DrawLine(transform.position, hitInfo.point, Color.red, 100f);
                        rhp.Raise(true);
                    }
                    else
                        rhp.Raise(false);
                }
            }
            
            yield return new WaitForSeconds(0.5f);
        }

    }
}

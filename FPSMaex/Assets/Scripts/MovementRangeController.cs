using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MovementRangeController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController>().dispararRaycast(true);
        }
    }


    public void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController>().setTarget(3);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController>().setTarget(-1);
            transform.parent.GetComponent<NavMeshController>().dispararRaycast(false);
        }
    }
}

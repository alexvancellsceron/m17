using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ControlTamano : ScriptableObject
{
    public bool disminuirTamano = false;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public GameObject puerta;
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Interruptor")
            StartCoroutine(moverPuerta(collision));
    }

    IEnumerator moverPuerta(Collision collision)
    {
        puerta.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        puerta.transform.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 1, 0);
        puerta.transform.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        yield return new WaitForSeconds(3);
        puerta.transform.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        puerta.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }
}

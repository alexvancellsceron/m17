using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairClimb : MonoBehaviour
{
    Rigidbody rigidBody;
    [SerializeField] Transform stepRayUpper;
    [SerializeField] Transform stepRayLower;
    [SerializeField] float stepHeight = .1f;
    [SerializeField] float stepSmooth = 0.01f;
    

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();

        stepRayUpper.position = new Vector3(stepRayUpper.position.x, stepHeight, stepRayUpper.position.z);
    }

    private void FixedUpdate()
    {
        stepClimb();
    }

    void stepClimb()
    {
        RaycastHit hitLower;
        RaycastHit hitUpper;
        if (Physics.Raycast(stepRayLower.position, transform.TransformDirection(Vector3.forward), out hitLower, 0.5f))
        {
            
            if (!Physics.Raycast(stepRayUpper.position, transform.TransformDirection(Vector3.forward), out hitUpper, 0.7f))
            {
                rigidBody.position = new Vector3(rigidBody.transform.position.x, rigidBody.transform.position.y + stepSmooth, rigidBody.transform.position.z);
            }
        }

        Debug.DrawRay(transform.GetChild(0).transform.position, transform.GetChild(0).transform.forward, Color.red);
        Debug.DrawRay(transform.GetChild(1).transform.position, transform.GetChild(1).transform.forward, Color.green);

        /*
        RaycastHit hitLower45;
        if (Physics.Raycast(stepRayLower.position, transform.TransformDirection(1.5f, 0, 1), out hitLower45, 0.1f))
        {

            RaycastHit hitUpper45;
            if (!Physics.Raycast(stepRayUpper.position, transform.TransformDirection(1.5f, 0, 1), out hitUpper45, 0.2f))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }

        RaycastHit hitLowerMinus45;
        if (Physics.Raycast(stepRayLower.position, transform.TransformDirection(-1.5f, 0, 1), out hitLowerMinus45, 0.1f))
        {

            RaycastHit hitUpperMinus45;
            if (!Physics.Raycast(stepRayUpper.position, transform.TransformDirection(-1.5f, 0, 1), out hitUpperMinus45, 0.2f))
            {
                rigidBody.position -= new Vector3(0f, -stepSmooth * Time.deltaTime, 0f);
            }
        }
        */
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptMoverPlatform : MonoBehaviour
{
    Rigidbody rb;
    Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        target = new Vector3(-125.61f, 45.10186f, -36.59858f);
        rb = GetComponent<Rigidbody>();
        CorrutinaGuardada = StartCoroutine(mover());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Coroutine CorrutinaGuardada;
    public void determinarBool(bool b)
    {
        if (b)
        {
            target = new Vector3(-90.5f, 45.10186f, -36.59858f);
        }
        else
        {
            target = new Vector3(-125.61f, 45.10186f, -36.59858f);
        }
            
    }

    IEnumerator mover()
    {
        
        float time = 0;
        yield return new WaitForSeconds(0.5f);
        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, target, time / 10000);
            time += Time.deltaTime;
            yield return null;
        }
        
    }
}

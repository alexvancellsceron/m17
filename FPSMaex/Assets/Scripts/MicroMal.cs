using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class MicroMal : MonoBehaviour
{
    private CharacterController cc;
    public float m_Spd = 0.1f;
    private Vector3 m_Velocity;
    private GameObject m_Target;
    private float m_ShootCD = 1.5f;
    private Vector3 m_ShootOrigin;
    private Vector3 movement;
    NavMeshAgent agent;
    Transform target;


    // Start is called before the first frame update
    void Start()
    {       
        cc = GetComponent<CharacterController>();
        StartCoroutine(ShootCD());
        StartCoroutine(modifyRotation());
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        m_ShootOrigin = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);
        if(m_Target != null)
            transform.LookAt(m_Target.transform.position);
    }

    IEnumerator modifyRotation()
    {
        int i;
        while (true)
        {
            i = Random.Range(-1, 2);
            cc.transform.Rotate(0, i, 0);
            yield return new WaitForSeconds(.5f);
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        movement = transform.forward;
        cc.Move(movement * m_Spd);
        if (!cc.isGrounded)
        {
            m_Velocity.y += -9.8f;
        }
        else
        {
            m_Velocity.y = 0f;
        }
        cc.Move(m_Velocity);
    }

    public void setTarget(Transform targetT)
    {
        target = targetT;
    }
    
    public void Shoot()
    {
        Debug.DrawRay(m_ShootOrigin, transform.forward * 10, Color.green);
        RaycastHit hit;
        if(Physics.Raycast(m_ShootOrigin, transform.forward, out hit))
        {
            print(hit.distance);
        }
    }

    IEnumerator ShootCD()
    {
        while(m_Target!=null)
        {
            yield return new WaitForSeconds(m_ShootCD);
            Shoot();
        }
        
    }
}

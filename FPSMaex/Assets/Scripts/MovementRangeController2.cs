using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementRangeController2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController2>().dispararRaycast(true);
        }
    }

    public void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController2>().setTarget(8);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            transform.parent.GetComponent<NavMeshController2>().setTarget(-1);
            transform.parent.GetComponent<NavMeshController2>().dispararRaycast(false);

        }
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public MoverPlatform moverPlatformSO;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(laser());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator laser()
    {
        RaycastHit hitInfo;
        int rango = 10;
        while(true)
        {
            if (Physics.Raycast(this.transform.position, -this.transform.right, out hitInfo, rango))
            {
                if(hitInfo.transform.tag == "Player")
                {
                    print(hitInfo.transform.name);
                    moverPlatformSO?.Raise(true);
                }
                else
                    moverPlatformSO?.Raise(false);
            }
            else
            {
                moverPlatformSO?.Raise(false);
            }
                

            Debug.DrawLine(this.transform.position, new Vector3(this.transform.position.x, this.transform.position.y - 10, this.transform.position.z), Color.blue, 100f);
            yield return null;
        }
        
    }
}

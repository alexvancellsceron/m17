using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class Player : NetworkBehaviour
{
    Rigidbody rb;
    public Transform rodillas, pies, mainCam;
    public RawImage mirilla;
    public CinemachineVirtualCamera camPOVapuntar, camPOV;
    public GameObject armaTamano, armaPupa, armaGancho, barraHP, barraSTAMINA;
    Vector3 respawn1, respawn2, respawn3;
    public GameObject ragdoll;
    float gravmod;
    FPSMaex inputActions;

    NetworkVariable<float> vidaHPValor = new NetworkVariable<float>();
    NetworkVariable<float> staminaValor = new NetworkVariable<float>();

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();

        gancho = false;
        tamano = false;
        pupa = true;
        //Cursor.lockState = CursorLockMode.Locked;
        respawn1 = new Vector3(-122.57f, 40.52f, -126.7f);
        respawn2 = new Vector3(-134.139f, 46.54f, -63.11f);
        respawn3 = new Vector3(-134.139f, 46.54f, -63.11f);

        impas1 = false;
        impas2 = false;

        delante = false;
        atras = false;
        izquierda = false;
        derecha = false;

        
    }

    public override void OnNetworkSpawn()
    {
        //rodillas.position = new Vector3(rb.transform.position.x, rb.transform.position.y, rb.transform.position.z); ^^
        //pies.position = new Vector3(rb.transform.position.x, rb.transform.position.y - 0.9f, rb.transform.position.z);
        //onvidaHPValueChange()
        mainCam = FindObjectOfType<Camera>().transform;
        camPOV = FindObjectsOfType<CinemachineVirtualCamera>().Where<CinemachineVirtualCamera>(component => component.name == "cameraPOV").First();
        camPOVapuntar = FindObjectsOfType<CinemachineVirtualCamera>().Where<CinemachineVirtualCamera>(component => component.name == "cameraPOVapuntar").First();
        barraHP = camPOV.transform.GetComponentsInChildren<Image>().Where<Image>(x => x.name == "barraHP").First().gameObject;
        barraSTAMINA = camPOV.transform.GetComponentsInChildren<Image>().Where<Image>(x => x.name == "barraStamina").First().gameObject;
        
        

        mirilla = camPOVapuntar.transform.GetComponentsInChildren<RawImage>().Where<RawImage>(x => x.name == "mirilla").First();

        if (IsServer)
        {
            vidaHPValor.Value = 1f;
            staminaValor.Value = 0f;

            StartCoroutine(subirStamina());
            StartCoroutine(subirHP());
        }

        if (!IsOwner)
            return;

        vidaHPValor.OnValueChanged += restarVida;
        staminaValor.OnValueChanged += restarStamina;
        

        inputActions = new FPSMaex();
        inputActions.Player.Enable();
        inputActions.Player.Apuntar.performed += btnApuntar;
        inputActions.Player.Apuntar.canceled += btnApuntar;
        inputActions.Player.Disparar.performed += btnDisparar;
        inputActions.Player.Disparar.canceled += btnDisparar;
        inputActions.Player.Correr.performed += btnCorrer;
        inputActions.Player.Correr.canceled += btnCorrer;
        inputActions.Player.CambiarArma.performed += btnCambiarArma;
        inputActions.Player.Saltar.performed += btnSaltar;

        //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale = new Vector3(0, 1, 1);

        SetPlayerPositionServerRpc(respawn1);
    }

    [ServerRpc]
    private void SetPlayerPositionServerRpc(Vector3 position)
    {
        transform.position = position;
        print(position);
    }

    IEnumerator subirStamina()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x < 1)
            {
                staminaValor.Value += 0.002f;
               // Debug.Log("Stmaina Value => " + staminaValor.Value);
            }
                
        }
    }

    IEnumerator subirHP()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            if (barraHP.transform.gameObject.GetComponent<RectTransform>().localScale.x < 1)
            {
                vidaHPValor.Value += 0.0001f;
            }
               
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (!IsOwner)
            return;

        camPOVapuntar.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y + .3f, rb.transform.position.z);
        camPOV.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y + .3f, rb.transform.position.z);
        armaTamano.transform.rotation = Quaternion.Euler(mainCam.transform.rotation.eulerAngles.x, rb.transform.rotation.eulerAngles.y, rb.transform.rotation.eulerAngles.z);
        armaPupa.transform.rotation = Quaternion.Euler(mainCam.transform.rotation.eulerAngles.x, rb.transform.rotation.eulerAngles.y, rb.transform.rotation.eulerAngles.z);
        armaGancho.transform.rotation = Quaternion.Euler(mainCam.transform.rotation.eulerAngles.x, rb.transform.rotation.eulerAngles.y, rb.transform.rotation.eulerAngles.z);

        rotarPlayerServerRpc(mainCam.transform.rotation.eulerAngles.y);
    }

    [ServerRpc]
    public void rotarPlayerServerRpc(float hectorRotationEnY)
    {
        transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, hectorRotationEnY, this.transform.rotation.eulerAngles.z);
    }

    private void FixedUpdate()
    {
        if (IsOwner)
        {
            Vector2 mov = inputActions.Player.Move.ReadValue<Vector2>();
            velPlayer = rb.transform.forward * spd * mov.y;
            velPlayer += rb.transform.right * spd * mov.x;
            ponmeLaVelocidadServerRpc(velPlayer);
        }

        if (IsServer)
        {
            if (rb.velocity.y < 0) rb.velocity = new Vector3(rb.velocity.x, -8, rb.velocity.z);
        }
    }

    [ServerRpc]
    private void ponmeLaVelocidadServerRpc(Vector3 v)
    {
        rb.velocity = new Vector3(v.x * 1.5f, rb.velocity.y, v.z * 1.5f);
    }

    bool gancho, tamano, pupa;
    float rango = 100;
    float momento = 3500;
    public void btnDisparar(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            //ejecucionBtnDispararserverRpc();
            
            RaycastHit hitInfo;
            int num = 1;
            Vector3 forward = mainCam.transform.TransformDirection(Vector3.forward);
            if (pupa)
            {
                num = 3;
                
                //forward = new Vector3(mainCam.transform.forward.x + Random.Range(-0.001f, 0.001f),
                //                mainCam.transform.forward.x + Random.Range(-0.001f, 0.001f),
                //                mainCam.transform.forward.z);
                
            }

            for (int i = 0; i < num; i++)
            {
                if (Physics.Raycast(rb.position, forward, out hitInfo, rango))
                {
                    if (pupa)
                    {
                        if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x - 0.1f >= 0)
                        {
                            if (IsServer)
                                staminaValor.Value += -.1f;
                            //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.1f, 0, 0);
                            if (hitInfo.transform.tag == "Pupa")
                            {
                                float dano = rango / hitInfo.distance;
                               
                                Vector3 tmpHitInfoNormal = hitInfo.normal;
                                Vector3 tmpHitInfoPoint = hitInfo.point;
                                String tmpHitInfoGO = hitInfo.transform.name;
                                moverAlDispararServerRpc(tmpHitInfoNormal, dano, tmpHitInfoPoint, tmpHitInfoGO);
                                //hitInfo.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hitInfo.normal * momento * dano, hitInfo.point);
                                Debug.DrawLine(mainCam.transform.position, hitInfo.point, Color.red, 100f);
                            }
                            if (hitInfo.transform.tag == "Ragdoll")
                            {
                                Quaternion r = hitInfo.transform.rotation;
                                Vector3 p = hitInfo.transform.position;
                                String tmpHitInfoGO = hitInfo.transform.name;
                                generarRagdollServerRpc(r, p, tmpHitInfoGO);

                            }
                        }

                    }
                    else if (tamano)
                    {
                        if (hitInfo.transform.tag == "Tamano")
                        {
                            if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x - 0.6f >= 0)
                            {
                                if (IsServer)
                                    staminaValor.Value += -.6f;
                                //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.6f, 0, 0);
                                if (hitInfo.transform.gameObject.GetComponent<ScriptTamano>().ct.disminuirTamano)
                                {
                                    //hitInfo.transform.localScale /= 2;
                                    String tmpHitInfoGO = hitInfo.transform.name;
                                    hacerPequenoServerRpc(tmpHitInfoGO);
                                    //hitInfo.transform.gameObject.GetComponent<ScriptTamano>().ct.disminuirTamano = false;
                                }
                                else
                                {
                                    if (hitInfo.transform.name != "PreFabTamano (4)")
                                    {
                                        //hitInfo.transform.localScale *= 2;
                                        String tmpHitInfoGO = hitInfo.transform.name;
                                        hacerGrandeServerRpc(tmpHitInfoGO);
                                        //hitInfo.transform.gameObject.GetComponent<ScriptTamano>().ct.disminuirTamano = true;
                                    }

                                }
                            }
                        }
                    }
                    else if (gancho)
                    {
                        if (hitInfo.transform.tag == "Enemy")
                        {
                            print("dentro enemy");
                        }
                        else if (hitInfo.transform.tag == "Gancho")
                        {
                            if (IsServer)
                                staminaValor.Value += -.8f;
                            if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x - 0.8f >= 0)
                            {
                                //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.8f, 0, 0);
                                CorrutinaGuardada = StartCoroutine(lerpMovimiento(hitInfo.point));
                            }

                        }

                    }
                }
            }
            
        }
        else if (c.canceled)
        {
            if (CorrutinaGuardada != null)
            {
                StopCoroutine(CorrutinaGuardada);
            }
        }

    }
    
    [ServerRpc]
    private void generarRagdollServerRpc(Quaternion q , Vector3 v , String s)
    {
        GameObject spawnedRagdollTransform = Instantiate(ragdoll, v, q);

        prueboAHacerFuncionesSinSentidoPorqueAsiLoExigeNetServerRpc(s);
        
    }

    [ServerRpc]
    private void prueboAHacerFuncionesSinSentidoPorqueAsiLoExigeNetServerRpc(String s)
    {
        Rigidbody g = FindObjectsOfType<Rigidbody>().Where<Rigidbody>(component => component.tag == "Pupa" && component.transform.name == s).First();
        g.transform.parent.gameObject.SetActive(false);
        //Debug.Log(g.transform.parent.gameObject);
    }

    [ServerRpc]
    private void moverAlDispararServerRpc(Vector3 hitInfo, float dano, Vector3 hitInfo2, String s)
    {
        Rigidbody g = FindObjectsOfType<Rigidbody>().Where<Rigidbody>(component => component.tag == "Pupa" && component.name == s).First();
        g.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hitInfo * momento * dano, hitInfo2);
    }
    
    [ServerRpc]
    private void hacerPequenoServerRpc(String s)
    {
        ScriptTamano g = FindObjectsOfType<ScriptTamano>().Where<ScriptTamano>(component => component.tag == "Tamano" && component.name == s).First();
        g.transform.localScale /= 2;
        g.ct.disminuirTamano = false;
    }
    
    [ServerRpc]
    private void hacerGrandeServerRpc(String s)
    {
        ScriptTamano g = FindObjectsOfType<ScriptTamano>().Where<ScriptTamano>(component => component.tag == "Tamano" && component.name == s).First();
        g.ct.disminuirTamano = true;
        g.transform.localScale *= 2;
    }
    
    Coroutine CorrutinaGuardada;

    IEnumerator lerpMovimiento(Vector3 target)
    {
        float time = 0;

        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, target, time/15);
            time += Time.deltaTime;
            //rb.velocity = new Vector3(0, rb.useGravity ? 0 : 0, 0);
            yield return null;
        }
    }

    public void btnApuntar(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            mirilla.gameObject.SetActive(true);
            camPOVapuntar.transform.GetComponent<CinemachineVirtualCamera>().Priority = 11;
        }
        else if(c.canceled)
        {
            mirilla.gameObject.SetActive(false);
            camPOVapuntar.transform.GetComponent<CinemachineVirtualCamera>().Priority = 9;
        }
            
    }

    public void btnCambiarArma(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            if(pupa)
            {
                pupa = false;
                tamano = false;
                gancho = true;
                armaGancho.gameObject.SetActive(true);
                armaPupa.gameObject.SetActive(false);
                armaTamano.gameObject.SetActive(false);
            }
            else if(gancho)
            {
                pupa = false;
                tamano = true;
                gancho = false;
                armaGancho.gameObject.SetActive(false);
                armaPupa.gameObject.SetActive(false);
                armaTamano.gameObject.SetActive(true);
            }
            else if(tamano)
            {
                pupa = true;
                tamano = false;
                gancho = false;
                armaGancho.gameObject.SetActive(false);
                armaPupa.gameObject.SetActive(true);
                armaTamano.gameObject.SetActive(false);
            }
        }
    }

    bool puedeSaltsar;

    public void btnSaltar(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x - 0.6f >= 0)
            {
                //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.6f, 0, 0);
                if (IsServer)
                    staminaValor.Value += -.6f;
                saltarServerRpc();
            }
        }
    }
    [ServerRpc]
    public void saltarServerRpc()
    {
        rb.AddForce(0, 350, 0);
    }

    bool impas1, impas2;

    public void OnCollisionEnter(Collision other)
    {
        if (!IsServer) return;
        if (other.transform.tag == "Respawn")
        {
            morir();
        }
        
    }

    private void OnCollisionStay(Collision other)
    {
        if (!IsServer) return;
        if (other.transform.tag == "Dolor")
        {
            //barraHP.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.01f, 0, 0);
            //restarVidaClientRpc(new ClientRpcParams { Send = new ClientRpcSendParams { TargetClientIds = new List<ulong> { GetComponent<NetworkObject>().OwnerClientId } } });
            vidaHPValor.Value += -.002f;
        }
            
    }

    public void morir()
    {
        if (impas2)
            rb.transform.position = respawn3;
        else if (impas1)
            rb.transform.position = respawn2;
        else
            rb.transform.position = respawn1;

        modificarGUIClientRpc(new ClientRpcParams { Send = new ClientRpcSendParams { TargetClientIds = new List<ulong> { GetComponent<NetworkObject>().OwnerClientId } } });
        vidaHPValor.Value = 1;
        staminaValor.Value = 1;
    }

    [ClientRpc]
    public void modificarGUIClientRpc(ClientRpcParams clientRpcParams)
    {

        //barraHP.transform.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Impas1")
            impas1 = true;
        if (other.transform.tag == "Impas2")
            impas2 = true;
    }

    public void restarVida(float previous, float current)
    {
        if (IsOwner)
            barraHP.transform.gameObject.GetComponent<RectTransform>().localScale = new Vector3(current, 1, 0);
    }


    public void restarStamina(float previous, float current)
    {
        Debug.Log("Current => "+current);
        if (IsOwner)
        {
            barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale = new Vector3(current, 1, 0);
            Debug.Log("Barra stmaina => "+barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale);
        }
            
    }

    Vector3 velPlayer, gravedad;
    int spd = 8;
    [SerializeField]
    bool delante, atras, derecha, izquierda;
    public void Mover(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            //gravedad = new Vector3(0, rb.useGravity ? -9 : 0, 0);
            if (c.ReadValue<Vector2>().y >= 0.5f)
            {
                atras = false;
                derecha = false;
                izquierda = false;
                delante = true;

            }
            else if (c.ReadValue<Vector2>().y <= -0.5f)
            {
                atras = true;
                derecha = false;
                izquierda = false;
                delante = false;
            }
            else if (c.ReadValue<Vector2>().x >= 0.5f)
            {
                atras = false;
                derecha = true;
                izquierda = false;
                delante = false;
            }
            else if (c.ReadValue<Vector2>().x <= -0.5f)
            {
                atras = false;
                derecha = false;
                izquierda = true;
                delante = false;
            }

        }
        else if (c.canceled)
        {
            atras = false;
            derecha = false;
            izquierda = false;
            delante = false;
            velPlayer = new Vector3(0, rb.velocity.y * gravmod, 0);
        }
    }

    Coroutine CorrutinaGuardada2;

    public void btnCorrer(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            CorrutinaGuardada2 = StartCoroutine(corriendo());
        }
        else if (c.canceled)
        {
            if (CorrutinaGuardada2 != null)
            {
                StopCoroutine(CorrutinaGuardada2);
            }
            spd = 8;
        }
    }

    IEnumerator corriendo()
    {
        while (true)
        {
            if (barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale.x - 0.0015f >= 0)
            {
                //barraSTAMINA.transform.gameObject.GetComponent<RectTransform>().localScale += new Vector3(-0.0015f, 0, 0);
                if (IsServer)
                    staminaValor.Value += -.0015f;
                spd = 15;
                yield return null;
            }
            else
            {
                spd = 8;
                yield return null;
            }

        }
    }
}

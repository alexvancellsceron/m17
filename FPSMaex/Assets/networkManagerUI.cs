using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class networkManagerUI : MonoBehaviour
{
    [SerializeField] private Button connect;
    [SerializeField] private Button host;
    [SerializeField] private Button server;

    private void Awake()
    {
        connect.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartClient();
            noquisistemataraestehombre();
        });
        host.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartHost();
            noquisistemataraestehombre();
        });
        server.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartServer();
            noquisistemataraestehombre();
        });
    }

    private void noquisistemataraestehombre()
    {
        connect.gameObject.SetActive(false);
        host.gameObject.SetActive(false);
        server.gameObject.SetActive(false);
    }
}

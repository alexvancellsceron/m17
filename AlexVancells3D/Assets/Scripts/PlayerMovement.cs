using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.GraphicsBuffer;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;
    public PlayerBoolSO bools;
    public PlayerTransformSO player;
    public PlayerRepositionsSO playerRepositions;
    public LevelSO lvl;
    Vector3 vel;
    public RespawnSO respawns;

    public delegate void avisoACanvas();
    public event avisoACanvas avisar;

    GameObject avisoGM, avisoGM2;

    private void Awake()
    {
        player.playerTransform = this.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        respawns.respawn = new Vector3(0, 3, 0);
        //respawns.respawn2 = new Vector3(0, 3, 117.5f);
        playerRepositions.restartTimes = 3;
        avisar.Invoke();
        vel = Vector3.zero;
        avisoGM = GameObject.Find("GameManager");
        avisoGM.GetComponent<GameManager>().avisar += PosicionarRotacionPlayer;
        avisoGM.GetComponent<GameManager>().avisarPlayerCanvas += avisarAAvisar;
    }

    public void avisarAAvisar()
    {
        avisar.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        player.playerTransform = this.transform;
        

    }

    public void PosicionarRotacionPlayer()
    {
        vel = Vector3.zero;
        playerRepositions.restartTimes--;
        avisar.Invoke();
        bools.canPosicionar = false;
        StartCoroutine(rotar());
    }

    Quaternion target;

    IEnumerator rotar()
    {
        rb.transform.position = Vector3.Lerp(this.transform.position, new Vector3(rb.position.x, rb.position.y + 1.5f, rb.position.z), 1);
        target = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(.1f);
        rb.transform.rotation = Quaternion.Lerp(rb.transform.rotation, target, 1f);
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.constraints = RigidbodyConstraints.None;
        bools.canMove = false;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Suelo" || collision.transform.tag == "ParaCubo")
        {
            StartCoroutine(restartPositionPlatform());
            bools.canMove = true;
            bools.canPosicionar = true;
        }
        else if (collision.transform.tag == "Muerte")
        {
            playerRepositions.restartTimes = 3;
            avisar.Invoke();
            bools.canPosicionar = false;
            StartCoroutine(rotar());
            this.transform.position = respawns.respawn;
            
        }
        else if (collision.transform.tag == "Fin")
        {
            respawns.respawn = new Vector3(0, 3, 0);
            playerRepositions.restartTimes = 3;
            lvl.lvl++;
            avisar.Invoke();
            bools.canPosicionar = false;
            StartCoroutine(rotar());
            this.transform.position = respawns.respawn;
        }
        else if (collision.transform.tag == "Rampa")
        {
            StartCoroutine(cancelarMove());
        }
    }
    IEnumerator cancelarMove()
    {
        bools.canMove = false;
        rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(.1f);
        bools.canMove = true;
    }

    public void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == "Rampa")
        {
            rb.velocity = Vector3.zero;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        rb.velocity = new Vector3(0, rb.useGravity ? -9 : 0, 0);
    }

    public delegate void avisoAPlatform();
    public event avisoAPlatform avisarPlatform;

    IEnumerator restartPositionPlatform()
    {
        yield return new WaitForSeconds(2);
        avisarPlatform.Invoke();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Respawn")
        {
            //print("RESPAWN");
            respawns.respawn = new Vector3(0, 3, 117.5f); ;
        }
    }

    public void test()
    {
        print(avisar);
    }
}

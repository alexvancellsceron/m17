using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    GameObject avisoDePlayer, avisDeSphere;
    // Start is called before the first frame update
    void Awake()
    {
        avisoDePlayer = GameObject.Find("Player");
        avisoDePlayer.GetComponent<PlayerMovement>().avisarPlatform += reseteame;

        avisDeSphere = GameObject.Find("SphereIker");
        avisDeSphere.GetComponent<SphereMovement>().avisarPlatform += reseteame;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reseteame()
    {
        //print("dentro platform");
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), 1);
        //transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}

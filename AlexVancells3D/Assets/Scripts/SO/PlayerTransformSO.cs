using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerTransformSO : ScriptableObject
{
    public Transform playerTransform;
    
}

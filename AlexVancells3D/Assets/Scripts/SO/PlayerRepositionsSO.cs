using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerRepositionsSO : ScriptableObject
{
    public int restartTimes = 3;
}

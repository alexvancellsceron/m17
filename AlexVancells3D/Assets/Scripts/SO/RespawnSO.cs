using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RespawnSO : ScriptableObject
{
    public Vector3 respawn;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerBoolSO : ScriptableObject
{
    public bool canPosicionar;
    public bool canMove;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LerpCamera : MonoBehaviour
{

    public PlayerTransformSO player, sphere;
    Transform target;
    int state;

    Coroutine c_guardada;

    GameObject avisoGM;

    // Start is called before the first frame update
    void Start()
    {
        state = 1;
        this.transform.Rotate(20, 0, 0);
        target = player.playerTransform;
        c_guardada = StartCoroutine(LerpPositionAmbSlowDown());
        avisoGM = GameObject.Find("GameManager");
        avisoGM.GetComponent<GameManager>().avisarCamera += cambiarTarget;
    }

    public void cambiarTarget(bool b)
    {
        if(b)
            target = player.playerTransform;
        
        else
            target = sphere.playerTransform;
    }

    IEnumerator LerpPositionAmbSlowDown()
    {
        float time = 0;

        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y + 5,
                target.position.z - 11), (Time.deltaTime * 3f));
            time += (Time.deltaTime * 3f);

            yield return null;
        }

    }


    
    public void rotarCameraQ(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            //print("Q "+state);
            if(state==2)
            {
                state--;
                this.transform.Rotate(20, 90, -20);
                StopCoroutine(c_guardada);
                c_guardada = StartCoroutine(LerpPositionAmbSlowDown());
            }
            else if (state == 1)
            {
                state--;
                this.transform.Rotate(20, 90, -20);
                StopCoroutine(c_guardada);
                c_guardada = StartCoroutine(izquierda());
            }
        }

    }

    IEnumerator izquierda()
    {
        float time = 0;

        while (true)
        {
            
            transform.position = Vector3.Lerp(transform.position, new Vector3(
                target.transform.position.x - 11, 
                target.transform.position.y + 5,
                target.position.z), (Time.deltaTime * 3f));
            time += (Time.deltaTime * 3f);

            yield return null;
        }
    }

    public void rotarCameraE(InputAction.CallbackContext c)
    {
        //print("E "+state);
        if (c.performed)
        {
            
            if (state == 1)
            {
                state++;
                this.transform.Rotate(20, -90, 20);
                StopCoroutine(c_guardada);
                c_guardada = StartCoroutine(derecha());
            }
            else if (state == 0)
            {
                state++;
                this.transform.Rotate(20, -90, 20);
                StopCoroutine(c_guardada);
                c_guardada = StartCoroutine(LerpPositionAmbSlowDown());
            }
        }
    }

    IEnumerator derecha()
    {
        float time = 0;

        while (true)
        {

            transform.position = Vector3.Lerp(transform.position, new Vector3(
                target.transform.position.x + 11,
                target.transform.position.y + 5,
                target.position.z), (Time.deltaTime * 3f));
            time += (Time.deltaTime * 3f);

            yield return null;
        }
    }


    private void Update()
    {
        
    }


}

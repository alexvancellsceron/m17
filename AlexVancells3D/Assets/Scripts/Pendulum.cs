using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    JointMotor motor;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(stillGo());
    }

    IEnumerator stillGo()
    {
        yield return new WaitForSeconds(1);
        motor = this.GetComponent<HingeJoint>().motor;
        motor.force = 0;
        this.GetComponent<HingeJoint>().motor = motor;
    }
}

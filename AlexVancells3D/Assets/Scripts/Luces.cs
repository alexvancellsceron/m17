using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Luces : MonoBehaviour
{
    public GameObject player, sphere;
    public PlayerTransformSO pT, sT;
    RaycastHit hitInfo;
    public DiaNocheSO dia;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(player.activeSelf)
        {
            this.transform.LookAt(pT.playerTransform);
            if (Mathf.Abs(this.transform.position.z - pT.playerTransform.position.z) >= this.GetComponent<Light>().range || dia.dia)
            {
                this.GetComponent<Light>().enabled = false;
            }
            else
                accionarLight();

        }
        else if(sphere.activeSelf)
        {
            this.transform.LookAt(sT.playerTransform);
            if (Mathf.Abs(this.transform.position.z - sT.playerTransform.position.z) >= this.GetComponent<Light>().range || dia.dia)
            {
                this.GetComponent<Light>().enabled = false;
            }
            else
                accionarLight();
        }

        
        Debug.DrawRay(this.transform.position, this.transform.forward, Color.cyan);
        
    }

    public void accionarLight()
    {
        if (Physics.SphereCast(this.transform.position, 1f, this.transform.forward, out hitInfo))
        {
            if ((hitInfo.transform.name == "Player" || hitInfo.transform.name == "SphereIker") && !dia.dia)
            {
                this.GetComponent<Light>().enabled = true;
                //print("Name rayCast " + this.transform.name);
                //print("dia? "+dia.dia);
            }
        }
    }
}

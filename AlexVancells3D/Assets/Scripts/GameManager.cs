using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static GameManager;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject player, sphere;
    Rigidbody p, s;
    public PlayerBoolSO bools;
    public PlayerRepositionsSO playerRepositions;
    public LevelSO lvl;
    Vector3 velPlayer, velSphere;

    public delegate void avisoAPlayer();
    public event avisoAPlayer avisar;
    public event avisoAPlayer avisarPlayerCanvas;

    public delegate void avisoASphere();
    public event avisoASphere avisarSphere;
    public event avisoASphere avisarSphereCanvas;

    public delegate void avisoACamera(bool b);
    public event avisoACamera avisarCamera;

    public delegate void avisoACubeMove();
    public event avisoACubeMove avisarCubeMove;

    public GameObject[] tramo1, tramo2;
    int lvlAux;

    private void Awake()
    {
        bools.canMove = true;
        bools.canPosicionar = true;
        lvl.lvl = 1;
        lvlAux = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        p = player.GetComponent<Rigidbody>();
        s = sphere.GetComponent<Rigidbody>();

        sphere.SetActive(false);
        velPlayer = new Vector3(0, p.useGravity? -3 : 0, 0);
        velSphere = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (lvlAux != lvl.lvl)
        {
            lvlAux = lvl.lvl;
            //print("generar lvl nuevo");
            generarNivelNuevo();
        }
        if (lvl.lvl==4)
        {
            //WIN
            print("WIN");
        }
        if(sphere.activeSelf)
        {
            s.AddTorque(new Vector3(velSphere.y, 0, -velSphere.x), ForceMode.Acceleration);
        }
        if(player.activeSelf)
        {
            p.velocity = velPlayer;
        }
    }

    public void generarNivelNuevo()
    {
        for(int i = 0; i < tramo1.Length; i++)
        {
            tramo1[i].SetActive(false);
        }
        for (int i = 0; i < tramo2.Length; i++)
        {
            tramo2[i].SetActive(false);
        }
        StartCoroutine(generarNivel());
    }

    IEnumerator generarNivel()
    {
        yield return new WaitForSeconds(1);
        List<int> l = new List<int>();
        for (int i = 0; i < lvl.lvl; i++)
        {
            int r = Random.Range(0, 3);

            while(l.Contains(r))
            {
                r = Random.Range(0, 3);
            }
            l.Add(r);
            
        }


        for(int i = 0; i < l.Count; i++)
        {
            tramo1[l[i]].SetActive(true);
        }
        for (int i = 0; i < l.Count; i++)
        {
            tramo2[l[i]].SetActive(true);
            if(i==0)
            {
                avisarCubeMove.Invoke();
            }
        }
    }

    public void Mover(InputAction.CallbackContext c)
    {
        
        if (c.performed && bools.canMove)
        {
            //print("Read value: "+c.ReadValue<Vector2>());
            //print("forward: "+this.transform.forward);
            //print("right: "+this.transform.right);
            if (player.activeSelf)
            {
                velPlayer = new Vector3(0, p.useGravity ? -1 : 0, 0);
                if (c.ReadValue<Vector2>().y >= 0.5f)
                {
                    velPlayer += player.transform.parent.transform.forward * 4;
                    
                }
                else if (c.ReadValue<Vector2>().y <= -0.5f)
                {
                    velPlayer += p.transform.parent.transform.forward * -4;
                    
                }
                else
                {
                    velPlayer += new Vector3(c.ReadValue<Vector2>().x, c.ReadValue<Vector2>().y, 0) * 4;
                }
                //p.AddTorque(vel);
                //p.velocity= vel;
            }
            else if(sphere.activeSelf)
            {
                //print(c.ReadValue<Vector2>());
                velSphere = c.ReadValue<Vector2>()*10;
            }
        }
        else if(c.canceled)
        {
            if (player.activeSelf)
            {
                velPlayer = new Vector3(0, p.useGravity? -9 : 0, 0);
            }
            else
            {
                velSphere = Vector3.zero;
            }
        }
    }

    public void PosicionarRotation(InputAction.CallbackContext c)
    {

        if (c.performed && bools.canPosicionar && playerRepositions.restartTimes > 0)
        {
            if (player.activeSelf)
            {
                avisar.Invoke();
            }
            else
            {
                avisarSphere.Invoke();
            }
        }
    }

    public void CambiarPlayer(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            if (player.activeSelf)
            {
                sphere.SetActive(true);
                p.velocity = Vector3.zero;
                s.velocity = Vector3.zero;
                sphere.transform.position = player.transform.position;
                StartCoroutine(esperar());
                player.SetActive(false);
                avisarCamera.Invoke(false);
            }
            else if (!player.activeSelf)
            {
                player.SetActive(true);
                s.velocity = Vector3.zero;
                p.velocity = Vector3.zero;
                player.transform.position = sphere.transform.position;
                avisarPlayerCanvas.Invoke();
                sphere.SetActive(false);
                avisarCamera.Invoke(true);
            }
        }
    }

    IEnumerator esperar()
    {
        yield return new WaitForSeconds(.1f);
        avisarSphereCanvas.Invoke();
    }
}

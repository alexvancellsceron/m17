using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCube : MonoBehaviour
{
    Vector3 target;
    Rigidbody rb;
    GameObject avisoGM;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        target = new Vector3(-15, 3.7f, 138);
        StartCoroutine(Move());
        avisoGM = GameObject.Find("GameManager");
        avisoGM.GetComponent<GameManager>().avisarCubeMove += MoveFunction;
    }

    public void MoveFunction()
    {
        StartCoroutine(Move());
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.x <= -15)
            target = new Vector3(25f, 3.7f, 138);

        if (transform.position.x >= 15)
            target = new Vector3(-25f, 3.7f, 138);

    }

    IEnumerator Move()
    {
        //print("dentro move " + target + " " + rb.transform.position);
        yield return new WaitForSeconds(1);
        while (true)
        {
            rb.transform.position = Vector3.Lerp(rb.transform.position, target, Time.deltaTime / 3);
            yield return null;
        }
    }
}

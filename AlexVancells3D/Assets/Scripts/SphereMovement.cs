using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMovement : MonoBehaviour
{
    Rigidbody rb;
    public PlayerBoolSO bools;
    public PlayerTransformSO sphere;
    public PlayerRepositionsSO playerRepositions;
    public LevelSO lvl;
    Vector3 vel;
    public RespawnSO respawns;

    public delegate void avisoACanvasSphere();
    public event avisoACanvasSphere avisar2;

    GameObject avisoGM;

    private void Awake()
    {
        sphere.playerTransform = this.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        vel = Vector3.zero;
        playerRepositions.restartTimes = 3;
        avisoGM = GameObject.Find("GameManager");
        avisoGM.GetComponent<GameManager>().avisarSphere += PosicionarRotacionPlayer;
        avisoGM.GetComponent<GameManager>().avisarSphereCanvas += avisarAAvisar;
    }

    public void avisarAAvisar()
    {
        avisar2.Invoke();
        rb.constraints = RigidbodyConstraints.None;
    }

    // Update is called once per frame
    void Update()
    {
        sphere.playerTransform = this.transform;
    }

    public void PosicionarRotacionPlayer()
    {
        vel = Vector3.zero;
        playerRepositions.restartTimes--;
        avisar2.Invoke();
        bools.canPosicionar = false;
        StartCoroutine(rotar());
    }

    Quaternion target;

    IEnumerator rotar()
    {
        rb.transform.position = Vector3.Lerp(this.transform.position, new Vector3(rb.position.x, rb.position.y + 1.5f, rb.position.z), 1);
        target = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(.1f);
        rb.transform.rotation = Quaternion.Lerp(rb.transform.rotation, target, 1f);
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.constraints = RigidbodyConstraints.None;
        bools.canMove = false;
    }

    public void OnCollisionEnter(Collision collision)
    {
        
        if (collision.transform.tag == "Suelo" || collision.transform.tag == "Rampa")
        {
            StartCoroutine(restartPositionPlatform());
            bools.canMove = true;
            bools.canPosicionar = true;
        }
        else if (collision.transform.tag == "Muerte")
        {
            playerRepositions.restartTimes = 3;
            avisar2.Invoke();
            bools.canPosicionar = false;
            StartCoroutine(rotar());
            this.transform.position = respawns.respawn;

        }
        else if (collision.transform.tag == "Fin")
        {

            respawns.respawn = new Vector3(0, 3, 0); ;
            playerRepositions.restartTimes = 3;
            lvl.lvl++;
            avisar2.Invoke();
            bools.canPosicionar = false;
            StartCoroutine(rotar());
            this.transform.position = respawns.respawn;

        }
        else if(collision.transform.tag == "ParaCubo")
        {
            StartCoroutine(cancelarMove());
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        
    }

    public delegate void avisoAPlatform();
    public event avisoAPlatform avisarPlatform;

    IEnumerator restartPositionPlatform()
    {
        yield return new WaitForSeconds(2);
        avisarPlatform.Invoke();
    }

    IEnumerator cancelarMove()
    {
        bools.canMove = false;
        rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(.1f);
        bools.canMove = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Respawn")
        {
            //print("RESPAWN");
            respawns.respawn = new Vector3(0, 3, 117.5f);
        }
    }

    public void test()
    {
        print(avisar2);
    }
}

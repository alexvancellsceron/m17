using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class CicloDia : MonoBehaviour
{

    public DiaNocheSO dia;
    void Update()
    {
        this.transform.RotateAround(new Vector3(0, 0, 0), this.transform.right, 10 * Time.deltaTime);
        //print("rotation directional light "+this.transform.rotation.x);
        if(Mathf.Abs(this.transform.rotation.x) > 0.96)
        {
            print("noche");
            dia.dia = false;
        }
        else if(Mathf.Abs(this.transform.rotation.x) < 0.05)
        {
            print("dia");
            dia.dia = true;
        }
    }


}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasUptade : MonoBehaviour
{
    public PlayerRepositionsSO playerRepositions, sphereRepositions;
    public LevelSO lvl;
    GameObject avisoDelegadoSphere;
    GameObject avisoDelegadoPlayer;
    

    public GameObject[] visibles;
    public GameObject txt1, txtLvl;

    // Start is called before the first frame update
    // Start is called before the first frame update
    // Start is called before the first frame update
    void Awake()
    {
        avisoDelegadoPlayer = GameObject.Find("Player");
        avisoDelegadoPlayer.GetComponent<PlayerMovement>().avisar += updateame;
        //avisoDelegadoPlayer.GetComponent<PlayerMovement>().test();
        avisoDelegadoSphere = GameObject.Find("SphereIker");
        //print(avisoDelegadoSphere.tag);
        avisoDelegadoSphere.GetComponent<SphereMovement>().avisar2 += updateame2;
        //avisoDelegadoSphere.GetComponent<SphereMovement>().test();
    }

    public void updateame()
    {
        visibles[0].SetActive(false);
        visibles[1].SetActive(false);
        visibles[2].SetActive(false);
        visibles[3].SetActive(false);
        visibles[4].SetActive(false);
        visibles[5].SetActive(false);
        for (int i = 0; i<playerRepositions.restartTimes; i++)
        {
            visibles[i].SetActive(true);
        }
        txtLvl.GetComponent<TextMeshProUGUI>().text = "Level " + lvl.lvl;
    }

    public void updateame2()
    {
        visibles[0].SetActive(false);
        visibles[1].SetActive(false);
        visibles[2].SetActive(false);
        visibles[3].SetActive(false);
        visibles[4].SetActive(false);
        visibles[5].SetActive(false);
        for (int i = 3; i < sphereRepositions.restartTimes+3; i++)
        {
            visibles[i].SetActive(true);
        }
        txtLvl.GetComponent<TextMeshProUGUI>().text = "Level " + lvl.lvl;
    }
}

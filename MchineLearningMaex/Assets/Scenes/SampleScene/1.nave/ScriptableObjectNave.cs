using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableObjectNave : ScriptableObject
{
    /*
     * Contenidor per a que els prefab puguin accedir a la posicio en x del GameObject nave.
     */
    public int px;
}

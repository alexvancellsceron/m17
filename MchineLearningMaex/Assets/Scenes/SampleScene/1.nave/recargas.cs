using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class recargas : MonoBehaviour
{
    public ScriptableObjectNave posx;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        /*
         * A partir d'un ScriptableObject calcula la seva posici� i es destrueix.
         */
        if (this.GetComponent<Rigidbody2D>().position.x < posx.px - 25)
        {
            Destroy(this.gameObject);
        }
    }

    /*
     * Si entra en contacte amb el tag, el destrueix.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}

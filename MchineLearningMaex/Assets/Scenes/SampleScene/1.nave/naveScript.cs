using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using static naveScript;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using UnityEditor.Presets;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.SceneManagement;

public class naveScript : Agent
{
    Rigidbody2D rb;
    ScoreTime scoreTime;
    Spawner s;
    public GameObject[] humos;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(3, 0);
        scoreTime = GameObject.Find("TextoCanvasScore").GetComponent<ScoreTime>();
        s = GameObject.Find("Main Camera").GetComponent<Spawner>();

        
    }
    Coroutine cg;
    IEnumerator subirPuntosAlimentaciosParaAlbert()
    {
        while(!this.gameObject.IsDestroyed())
        {
            yield return new WaitForSeconds(1);
            AddReward(+1f);
           //Debug.Log("Puntos acumulados, dos puntos, get cum: "+GetCumulativeReward());
        }
        
    }

    private float forcey = 3;
    public int estabilizador;
    public int turboValor;
    private bool estabilizadorBool = false;
    public ScriptableObjectNave PosicionNave;
    public GameObject[] escenarios;
    private GameObject escenario0;
    private GameObject escenario1;
    private GameObject escenario2;
    public GameObject nodriza;
    private float generarDeX = 140;
    float vx;
    float vy;

    // Update is called once per frame
    void Update()
    {
        /*
        humo1.transform.position = new Vector2(rb.position.x - 0.5f, rb.position.y + 0.35f);
        humo1.GetComponent<Rigidbody2D>().velocity = new Vector2(rb.velocity.x, rb.velocity.y);

        humo2.transform.position = new Vector2(rb.position.x - 0.5f, rb.position.y);
        humo2.GetComponent<Rigidbody2D>().velocity = new Vector2(rb.velocity.x, rb.velocity.y);
        */
        vx = rb.velocity[0];
        vy = rb.velocity[1];
        PosicionNave.px = (int)rb.position.x;

        /*
         * if -> genera nou escenari a partir de la posici� de la nau
         */
        if (Math.Floor((float)PosicionNave.px) == generarDeX && PosicionNave.px >= 10)
        {
            generarDeX += 140;

            int ran = UnityEngine.Random.Range(0, 3);
            if (ran == 0)
            {
                escenario0 = Instantiate(escenarios[0]);
                escenario0.transform.position = new Vector2(PosicionNave.px + (150 - 9.619995f), 0.6048889f);
            }
            if (ran == 1)
            {
                escenario1 = Instantiate(escenarios[1]);
                escenario1.transform.position = new Vector2(PosicionNave.px + (150 - 9.619995f), 0.6048889f);
            }
            if (ran == 2)
            {
                escenario2 = Instantiate(escenarios[2]);
                escenario2.transform.position = new Vector2(PosicionNave.px + (150 - 9.619995f), 0.6048889f);
            }

        }
        RequestDecision();
        
        

        
        

        
        

        /*
         * procura mantenir una velocitat m�nima alhora que fregament linear, i desbloqueja la rotaci� en tots els eixos.
         */
        if (vx >= 3)
        {
            rb.drag = 0.3f;
            rb.constraints = RigidbodyConstraints2D.None;
            rb.velocity = new Vector2(vx * transform.right.x, vy);
        }
        else
        {
            rb.drag = 0;
            rb.velocity = new Vector2(3 * transform.right.x, vy);
        }

        

    }

    public override void OnActionReceived(ActionBuffers ab)
    {
        //Debug.Log(ab.DiscreteActions[0]+" "+ ab.DiscreteActions[1]);
        if (ab.DiscreteActions[0] == 1 && ab.DiscreteActions[1] == 0)
        {
            subir();
            Debug.Log("subir");
        }
        else if (ab.DiscreteActions[0] == 1 && ab.DiscreteActions[1] == 1)
        {
            subir();
            turbo();
            Debug.Log("subir y turbo");
        }
        else if (ab.DiscreteActions[0] == 0 && ab.DiscreteActions[1] == 1)
        {
            turbo();
            Debug.Log("turbo");
        }
        else if (ab.DiscreteActions[0] == 1 && ab.DiscreteActions[1] == 2)
        {
            subir();
            rotar();
            Debug.Log("subir y rotar");
        }
        else if (ab.DiscreteActions[0] == 0 && ab.DiscreteActions[1] == 2)
        {
            rotar();
            Debug.Log("rotar");
        }

    }

    public void subir()
    {
        if (vy <= 6)
        {
            AddReward(+10f);
            rb.AddForce(new Vector2(0, forcey));
            //Debug.Log("subir");
        }
            
    }

    public void turbo()
    {
        if (turboValor >= 1)
        {
            
            if (vx <= 16)
            {
                turboValor--;
                AddReward(+5f);
                rb.AddForce(new Vector2(8 * transform.right.x, 0));
               // Debug.Log("turbo");
            }
        }
            
    }

    public void rotar()
    {
        if (estabilizador >= 1 && !estabilizadorBool && transform.localEulerAngles.z != 0)
        {
            AddReward(+5f);
            estabilizadorBool = true;
            estabilizador--;
            StartCoroutine(CRestabilizador());
            //Debug.Log("rotar");
        }
        
    }

    public override void OnEpisodeBegin()
    {
        Debug.Log(Mathf.Floor(scoreTime.temps));
        scoreTime.temps = 0;
        estabilizador = 3;
        turboValor = 1000;
        transform.position = Vector2.zero;
        rb.freezeRotation = true;
        rb.freezeRotation = false;
        s.aparicion = 5;
        foreach(GameObject go in s.spawnedObjects)
        {
            Destroy(go);
        }
        transform.localEulerAngles = Vector2.zero;
        if (cg != null) StopCoroutine(cg);
        cg = StartCoroutine(subirPuntosAlimentaciosParaAlbert());
        //OnReset?.Invoke();
    }




    /*
     *  Corrutina -> proporciona un augment de velocitat de rotaci� fins arribar a 0 graus/radiants.
     */
    private IEnumerator CRestabilizador()
    {
        while (Math.Floor(rb.rotation) % 360 != 0)
        {
            if (rb.rotation % 360 < 0)
            {
                rb.rotation += 1f;
                yield return new WaitForSeconds(0.0001f);
            }
            else
            {
                rb.rotation -= 1f;
                yield return new WaitForSeconds(0.0001f);
            }

        }
        rb.SetRotation(0);



        rb.velocity = new Vector2(3, 0);
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(1f);
        estabilizadorBool = false;


    }

    public void morir()
    {
        AddReward(-30f);
        EndEpisode();
    }

    /*
     * Col�lisi� -> regenera recursos al entrar en contacte amb un Collider2D (trigger)
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.transform.tag == "recargaEstabilizador")
        {
            estabilizador++;
            AddReward(+5);
        }
        if (collision.transform.tag == "recargaTurbo")
        {
            AddReward(+5);
            turboValor += 200;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
         AddReward(-20f);
        //Debug.Log("Choque");
    }
}

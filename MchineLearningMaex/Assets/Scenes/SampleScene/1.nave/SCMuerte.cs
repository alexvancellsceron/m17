using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SCMuerte : MonoBehaviour
{
    public GameObject nave;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    

    

    // Update is called once per frame
    void Update()
    {


    }

    /*
     * Comprova si l'objecte amb el tag el toca. El destrueix i canvia d'escena.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            //SceneManager.LoadScene(0);
            nave.GetComponent<naveScript>().morir();
            transform.position = new Vector2(-12.51001f, 0.7099915f);
        }
    }

}

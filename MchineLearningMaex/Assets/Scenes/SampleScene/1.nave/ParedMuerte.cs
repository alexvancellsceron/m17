using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ParedMuerte : MonoBehaviour
{
    public GameObject nave;
    Rigidbody2D ParedRG;
    ScoreTime scoreTime;


    // Start is called before the first frame update
    void Start()
    {
        ParedRG = this.GetComponent<Rigidbody2D>();
        scoreTime = FindObjectOfType<ScoreTime>();
    }

    // Update is called once per frame
    void Update()
    {

        /*
         * comprova que l'objecte del qual parteixen els seus calculs existeixi. Accedint a una altre classe proporciona m�s velocitat en x
         * a si mateix.
         */
        if (!nave.IsDestroyed())
        {
            if (ParedRG.position.x < nave.GetComponent<Rigidbody2D>().position.x - 35)
            {
                ParedRG.position = new Vector2(nave.GetComponent<Rigidbody2D>().position.x - 34, ParedRG.position.y);
            }
            if (Math.Floor(scoreTime.temps % 10) == 0 && scoreTime.temps >= 10)
            {
                float spd = (float)scoreTime.temps / 15;
                ParedRG.velocity = new Vector2(3.2f + spd, 0);
            }
            if (scoreTime.temps <= 10)
            {
                ParedRG.velocity = new Vector2(3.2f, 0);
            }
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboScript : MonoBehaviour
{
    naveScript turbo;


    // Start is called before the first frame update
    void Start()
    {
        turbo = FindObjectOfType<naveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * Actualitza, accedint a una altra classe, el seu contingut.
         */
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "" + turbo.turboValor;
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class muerteNodriza1 : MonoBehaviour
{
    Rigidbody2D nodriza1;
    public ScriptableObjectNave posx;

    // Start is called before the first frame update
    void Start()
    {
        nodriza1 = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * Controla, a partir d'un ScriptableObject, si s'ha de destruir per millorar el rendiment general.
         */
        if (nodriza1.position.x < posx.px - 140 && !(nodriza1.IsDestroyed()) && posx.px > 10)
        {
            Destroy(this.gameObject);
        }
    }
}

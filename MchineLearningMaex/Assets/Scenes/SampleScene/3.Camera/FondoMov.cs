using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FondoMov : MonoBehaviour
{
    public float spd;
    public Transform navePosition;
    public GameObject nave;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!nave.IsDestroyed())
        {
            if (this.transform.position.x < navePosition.position.x - 15)
            {
                this.transform.position = new Vector3(navePosition.position.x + 125, this.GetComponent<Rigidbody2D>().position.y, 0);
            }
        }


    }
}


using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float aparicion = 5;
    private int aparicion2 = 8;
    public GameObject[] items;
    private GameObject cometa;
    private GameObject roca;
    private GameObject recargaEstabilizador;
    private GameObject recargaTurbo;
    public List<GameObject> spawnedObjects = new List<GameObject>();
    ScoreTime tiempo;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnear());
        StartCoroutine(spawnear2());
        tiempo = FindObjectOfType<ScoreTime>();
      
    }

    // Update is called once per frame
    void Update()
    {

        /*
         * En funci� del temps actualitza la variable aparicion que determina la dificultat, en part, de l'escena.
         */
       if (System.Math.Floor(tiempo.temps % 10) == 0 && tiempo.temps >= 10 && aparicion >= 2)
        {
            aparicion -= (float)(tiempo.temps / 100000);
        }
    }


    /*
     * Mitjant�ant l'aleatorietat genera GameObjects que representen els enemics de l'escena.
     */
    IEnumerator spawnear()
    {
        while (true)
        {
            yield return new WaitForSeconds(aparicion);
            int ran = Random.Range(1, 10);
            float positiony = Random.Range(this.gameObject.transform.position.y + 4, this.gameObject.transform.position.y - 4);

            if (ran <= 4)
            {
                int spd = Random.Range(-10, -5);
                roca = Instantiate(items[1]);
                spawnedObjects.Add(roca);
                roca.transform.position = new Vector2(this.gameObject.transform.position.x + 25, positiony);
                roca.GetComponent<Rigidbody2D>().angularVelocity = 10;
                roca.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);

                if (ran >= 2)
                {
                    roca.transform.localScale = new Vector3(3.5f, 3.5f, 3.5f);
                    roca.GetComponent<Rigidbody2D>().angularVelocity = 50;
                }
                if (ran == 4)
                {
                    roca.transform.localScale = new Vector3(5.75f, 5.75f, 5.75f);
                    roca.GetComponent<Rigidbody2D>().angularVelocity = 20;
                }
                else
                {
                    roca.transform.localScale = new Vector3(6.25f, 6.25f, 6.25f);
                }

            }
            else
            {
                int spd = Random.Range(-20, -10);
                cometa = Instantiate(items[0]);
                spawnedObjects.Add(cometa);
                cometa.transform.position = new Vector2(this.gameObject.transform.position.x + 25, positiony);
                cometa.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);

                if (ran >= 7)
                {
                    cometa.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
                }
                if (ran >= 9)
                {
                    cometa.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
                }
                if (ran == 6)
                {
                    cometa.GetComponent<Rigidbody2D>().velocity = new Vector2(-32, 0);
                }
            }
        }
    }

    IEnumerator spawnear2()
    {
        while (true)
        {
            yield return new WaitForSeconds(aparicion2);
            int ran = Random.Range(1, 3);
            float positiony = Random.Range(this.gameObject.transform.position.y + 4, this.gameObject.transform.position.y - 4);

            if (ran == 1)
            {
                recargaEstabilizador = Instantiate(items[2]);
                spawnedObjects.Add(recargaEstabilizador);
                recargaEstabilizador.transform.position = new Vector2(this.gameObject.transform.position.x + 25, positiony);
                recargaEstabilizador.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
            }
            else
            {
                recargaTurbo = Instantiate(items[3]);
                spawnedObjects.Add(recargaTurbo);
                recargaTurbo.transform.position = new Vector2(this.gameObject.transform.position.x + 25, positiony);
                recargaTurbo.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
            }
        }
    }
}

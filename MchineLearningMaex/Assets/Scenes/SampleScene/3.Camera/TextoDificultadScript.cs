using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoDificultadScript : MonoBehaviour
{
    ScoreTime scoreTime;
    private bool ocultar = false;

    // Start is called before the first frame update
    void Start()
    {
        scoreTime = FindObjectOfType<ScoreTime>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * Actualitza, accedint a una altra classe, el seu contingut, i, acciona una corrutina.
         */
        if (Math.Floor(scoreTime.temps % 10) == 0 && scoreTime.temps >= 10 && !ocultar)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "¡DIFFICULTY INCREASED!";
            ocultar = true;
        }
        if (ocultar)
        {
            StartCoroutine(esperaAOcultar());
        }


    }

    /*
         * Actualitza el seu contingut a partir del temps.
         */
    IEnumerator esperaAOcultar()
    {
        yield return new WaitForSeconds(2);
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "";
        ocultar = false;
    }

}

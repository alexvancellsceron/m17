using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTime : MonoBehaviour
{
    public float temps = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        /*
         * Actualitza el seu contingut en funci� del temps.
         */
        temps += Time.deltaTime;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + Math.Floor(temps);

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstabilizadorScript : MonoBehaviour
{
    naveScript estabilizadoresRestantes;


    // Start is called before the first frame update
    void Start()
    {
        estabilizadoresRestantes = FindObjectOfType<naveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * Actualitza, accedint a una altra classe, el seu contingut.
         */
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "" + estabilizadoresRestantes.estabilizador;
    }
}

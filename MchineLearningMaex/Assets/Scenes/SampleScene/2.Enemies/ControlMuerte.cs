using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMuerte : MonoBehaviour
{
    public ScriptableObjectNave posx;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * A partir d'un ScriptableObject calcula la seva posici� i es destrueix.
         */
        if (rb.transform.position.x < posx.px - 25)
        {
            DestroyImmediate(this.gameObject, true);
        }
    }

}

using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ProceduralMap : MonoBehaviour
{
    public GameObject map;
    public GameObject water;
    public GameObject grass;
    public GameObject ground;
    public GameObject stone;
    public GameObject snow;

    public GameObject house;

    private GameObject bloquevacio;

    public int seed = 5000;
    public int cuevasSeed = 7500;
    public int planicieSeed = 10000;
    public int offsetX;
    public int offsetZ;

    [Range(0.0f, 1.0f)]
    public float probCuevas;

    [Range(0.0f, 1.0f)]
    public float probCasas;

    [Range(1f, 100f)]
    public float freqDesierto;

    [Range(2, 3)]
    public int lacunaridad;

    [Range(0.0f, 1.0f)]
    public float persuistencia;

    [Range(1f, 2000f)]
    public float scale;

    public float amp = 20f; // determina el valor de una variable, en este caso de y, la altura

    [Range(1, 100)]
    public float freq = 100.63f; // determina la dstancia focal al mapa procedural (zoom)



    // Start is called before the first frame update
    void Start()
    {
        amp *= persuistencia;
        freq *= lacunaridad;
        gererateProceduralMap();

    }

    public int fil = 100;
    public int col = 100;
    public int contadorCasas;

    private void gererateProceduralMap()
    {
        Vector3 pos = this.transform.position;

        //ens movem pel mapa com una matriu
        for (int x = 0; x < col; x++)
        {
            for (int z = 0; z < fil; z++)
            {
                
                float y = Mathf.PerlinNoise((float)((offsetX + x + seed) / scale * freq), (float)((offsetZ + z + seed) / scale * freq));
                float perlinCuevas = Mathf.PerlinNoise((float)((offsetX + x + cuevasSeed) / scale * freq), (float)((offsetZ + z + cuevasSeed) / scale * freq));
                float perlinBiomaDesierto = Mathf.PerlinNoise((float)((offsetX + x + cuevasSeed) / scale * freqDesierto), (float)((offsetZ + z + cuevasSeed) / scale *  freqDesierto));


                if(perlinBiomaDesierto >= .5f)
                {
                    y *= amp;
                    y = Mathf.Floor(y);
                    for (float i = 0; i < y + 1; i++)
                    {
                        if (i > amp * 0.6f)
                            bloquevacio = snow;
                        else if (i > amp * 0.45f)
                            bloquevacio = stone;
                        else
                            bloquevacio = grass;

                        int r = Random.Range(1, 4);

                        if (i <= r || i >= y - r || perlinCuevas > probCuevas)
                        {
                            GameObject bloqueVacioAInstnciar = Instantiate(bloquevacio);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }

                    }
                    for (float i = y; i < amp * 0.10f; i++)
                    {
                        GameObject bloqueVacioAInstnciar = Instantiate(water);
                        bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                        bloqueVacioAInstnciar.transform.parent = map.transform;
                    }
                }
                else if (perlinBiomaDesierto <= .3f)
                {
                    bool heEntrado = false;
                    bloquevacio = ground;
                    if(perlinBiomaDesierto < probCasas )
                    {
                        heEntrado = true;
                        float iker2 = perlinBiomaDesierto;
                        perlinBiomaDesierto *= amp / 2;
                        perlinBiomaDesierto = Mathf.Floor(perlinBiomaDesierto);
                        contadorCasas--;
                        if (contadorCasas <= 0 && iker2 >0.1f)
                        {
                            GameObject bloqueVacioAInstnciar = Instantiate(house);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, perlinBiomaDesierto, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }
                        

                        for (int i = 0; i < perlinBiomaDesierto; i++)
                        {
                            GameObject bloqueVacioAInstnciar = Instantiate(bloquevacio);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }
                    }
                    if (!heEntrado)
                    {
                        perlinBiomaDesierto *= amp / 2;
                        perlinBiomaDesierto = Mathf.Floor(perlinBiomaDesierto);
                    }
                    if (perlinBiomaDesierto >= probCasas)
                    {
                        for(int i = 0; i< perlinBiomaDesierto; i++)
                        {
                            GameObject bloqueVacioAInstnciar = Instantiate(bloquevacio);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }
                    }
                    for (float i = perlinBiomaDesierto; i < amp * 0.025f; i++)
                    {
                        GameObject bloqueVacioAInstnciar = Instantiate(water);
                        bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                        bloqueVacioAInstnciar.transform.parent = map.transform;
                    }
                }
                else
                {
                    y *= amp;
                    y = Mathf.Floor(y);

                    int iker =  (int) Mathf.Floor(((perlinBiomaDesierto - 0.3f) * 5 * y));
                    if (iker == 0) iker++;
                    if(iker >1)
                    {
                        for (int i = 0; i < iker; i++)
                        {
                            if (i > amp * 0.6f)
                                bloquevacio = snow;
                            else if (i > amp * 0.45f)
                                bloquevacio = stone;
                            else
                                bloquevacio = grass;
                            GameObject bloqueVacioAInstnciar = Instantiate(bloquevacio);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < iker+1; i++)
                        {
                            GameObject bloqueVacioAInstnciar = Instantiate(ground);
                            bloqueVacioAInstnciar.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                            bloqueVacioAInstnciar.transform.parent = map.transform;
                        }
                    }
                    
                }




                if (contadorCasas <= 0) contadorCasas = 10;



            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < map.transform.childCount; i++)
            {
                Destroy(map.transform.GetChild(i).gameObject);
            }
            gererateProceduralMap();
        }
    }
}

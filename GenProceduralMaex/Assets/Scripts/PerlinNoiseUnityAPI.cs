using Unity.VisualScripting;
using UnityEditor.PackageManager.UI;
using UnityEngine;
using UnityEngine.UI;

public class PerlinNoiseUnityAPI : MonoBehaviour
{
    [SerializeField]
    private bool m_Verbose = false;

    [SerializeField]
    private bool m_ApplyToTerrain;

    [SerializeField]
    //offset from the perlin map
    private float m_OffsetX;
    [SerializeField]
    private float m_OffsetY;

    //size of the area we will paint
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;

    //Scale
    [SerializeField]
    private float m_Scale = 1000f;
    //Scale
    [SerializeField]
    private float m_Frequency = 10f;

    //octaves
    private const int MAX_OCTAVES = 8;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;

    //graphic
    private Terrain m_Terrain;
    [SerializeField]
    private Gradient m_Gradient;
    private Texture2D[] m_Texture;

    //texture GUI
    [SerializeField]
    private GameObject m_GUI;
    private int m_TextureMode = 0;
    private int m_TextureOctave = 0;

    void Start()
    {
        m_Terrain = GetComponent<Terrain>();

        //Crearem una textura per cada perlin i les seves octaves amb el resultat conjunt
        //i un altre amb el resultat base.
        m_Texture = new Texture2D[(MAX_OCTAVES + 1) * 2];
        for (int i = 0; i < (MAX_OCTAVES + 1)*2; i++)
        {
            m_Texture[i] = new Texture2D(m_Width, m_Height);
            m_Texture[i].filterMode = FilterMode.Point;
        }
    }

    void Update()
    {
        //regenerar perlins
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GeneratePerlinMap();
            SelectTexture(m_TextureOctave, m_TextureMode);
        }

        //canviar textura entre base i combinada
        if (Input.GetKeyDown(KeyCode.C))
            SelectTexture(m_TextureOctave, (m_TextureMode + 1) % 2);

        //canviar l'octava de textura a mostrar
        if (Input.GetKeyDown(KeyCode.Alpha0))
            SelectTexture(0, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha1))
            SelectTexture(1, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            SelectTexture(2, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha3))
            SelectTexture(3, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha4))
            SelectTexture(4, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha5))
            SelectTexture(5, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha6))
            SelectTexture(6, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha7))
            SelectTexture(7, m_TextureMode);
        if (Input.GetKeyDown(KeyCode.Alpha8))
            SelectTexture(8, m_TextureMode);

    }

    private void GeneratePerlinMap()
    {
        SetUpTextures();
        float[,] heights = new float[m_Height,m_Width];
        Color[][] colors = new Color[(MAX_OCTAVES + 1) * 2][];
        for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
            colors[i] = new Color[m_Height * m_Width];

        float elapsedTime = Time.realtimeSinceStartup;
        Debug.Log("Calculant Perlin Noise");
        //recorrem el mapa
        for (int y = 0; y < m_Height; y++) 
        {
            for (int x = 0; x < m_Width; x++)
            {
                //per cada casella comprovem soroll perlin donats els par�metres
                //les coordenades x i y que buscarem venen despla�ades per l'offset
                //l'escala ens determina quan ens movem per unitat
                //la freq�encia ens determina com de continuus s�n els passos que fem
                float xCoord = m_OffsetX + (x / m_Scale) * m_Frequency;
                float yCoord = m_OffsetY + (y / m_Scale) * m_Frequency;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                //Valor base
                if(m_Verbose) Debug.Log(string.Format("Base: [{0},{1}] = {2}", x, y, sample));
                colors[0][x + y * m_Width] = m_Gradient.Evaluate(sample);

                //Acte seguit calculem les octaves
                for (int octave = 1; octave <= m_Octaves; octave++)
                {
                    //La Lacunarity afecta a la freq�encia de cada subseq�ent octava. El limitem a [2,3] de forma
                    //que cada nou valor sigui x2 o x3 de la freq�encia anterior (doble o triple de soroll)
                    float newFreq = m_Frequency * m_Lacunarity * octave;
                    float xOctaveCoord = m_OffsetX + (x / m_Scale) * newFreq;
                    float yOctaveCoord = m_OffsetY + (y / m_Scale) * newFreq;
                    
                    //valor base de l'octava
                    float octaveSample = Mathf.PerlinNoise(xOctaveCoord, yOctaveCoord);
                    colors[octave*2][x + y * m_Width] = m_Gradient.Evaluate(octaveSample);


                    //La Persistence afecta a l'amplitud de cada subseq�ent octava. El limitem a [0.1, 0.9] de forma
                    //que cada nou valor afecti menys al resultat final.
                    //addicionalment, farem que el soroll en comptes de ser un valor base [0,1] sigui [-0.5f,0.5f]
                    //i aix� pugui sumar o restar al valor inicial
                    octaveSample = (octaveSample - .5f) * (m_Persistence / octave);

                    //acumulaci� del soroll amb les octaves i base anteriors
                    if (m_Verbose) Debug.Log(string.Format("Octave {3}: [{0},{1}] = {2}", x, y, sample, octave));
                    sample += octaveSample;
                    colors[octave * 2 + 1][x + y * m_Width] = m_Gradient.Evaluate(sample);
                }

                if (m_Verbose) Debug.Log(string.Format("[{0},{1}] = {2}", x, y, sample));

                //i utilitzem el soroll com a factor per a determinar l'al�ada final del terreny
                heights[x,y] = sample;
                colors[1][x + y * m_Width] = m_Gradient.Evaluate(sample);
            }
        }

        Debug.Log(string.Format("Elapsed time for perlin calculations: {0}", Time.realtimeSinceStartup - elapsedTime));

        Debug.Log("Recreant el terreny");
        elapsedTime = Time.realtimeSinceStartup;
        if (m_ApplyToTerrain)
            m_Terrain.terrainData.SetHeights(0, 0, heights);

        Debug.Log(string.Format("Elapsed time for terrain generation: {0}", Time.realtimeSinceStartup - elapsedTime));
        elapsedTime = Time.realtimeSinceStartup;
        for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
        {
            m_Texture[i].SetPixels(colors[i]);
            m_Texture[i].Apply();
        }    
        Debug.Log(string.Format("Elapsed time for texture generation: {0}", Time.realtimeSinceStartup - elapsedTime));

        Debug.Log("Recreaci� finalitzada");
    }

    private void SetUpTextures()
    {
        for(int i=0; i < (MAX_OCTAVES + 1)*2; i++)
            m_Texture[i].Reinitialize(m_Width, m_Height);
    }

    private void SelectTexture(int octave, int mode)
    {
        m_TextureOctave = octave;
        m_TextureMode = mode;
        m_GUI.GetComponent<Renderer>().material.mainTexture = m_Texture[octave*2+mode];
        Debug.Log(string.Format("Mostrant la textura {0} de l'octava {1}", (m_TextureMode == 0 ? "base" : "combinada"), m_TextureOctave));
    }


}
